package fr.soleil.comete.csmonitor.model;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class ServerSource implements ISourceDevice {

    public static final String SEPARATOR = "#";
    private static final Map<String, List<String>> SERVER_MAP = new HashMap<>();
    private static List<String> serverList = null;

    private final ICSMonitor controller;

    public ServerSource(final ICSMonitor controller) {
        this.controller = controller;
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        ISourceDevice deviceSource = controller.getDeviceSource();
        if (serverList == null && deviceSource != null) {
            serverList = new ArrayList<>();
            List<String> sourceList = deviceSource.getSourceList();
            if (sourceList != null && !sourceList.isEmpty()) {
                Database database = TangoDeviceHelper.getDatabase();
                String[] get_server_list = null;
                if (database != null) {
                    try {
                        get_server_list = database.get_server_list();
                    } catch (DevFailed devFailed) {
                        controller.logError(this.getClass().getSimpleName(),
                                new Exception(DevFailedUtils.toString(devFailed)));
                    }
                    if (get_server_list != null && get_server_list.length > 0) {
                        String[] device_name_list = null;
                        String serverDeviceName = null;
                        List<String> deviceList = null;

                        int nbSource = get_server_list.length;
                        int i = 0;
                        int percent = 0;

                        for (String serverName : get_server_list) {
                            controller.setLoadingMessage("Loading server " + serverName);
                            deviceList = SERVER_MAP.get(serverName.toLowerCase());
                            if (deviceList == null) {
                                try {
                                    device_name_list = database.get_device_name(serverName, "*");
                                    if (device_name_list != null) {
                                        for (String deviceName : device_name_list) {
                                            if (sourceList.contains(deviceName.toLowerCase())) {
                                                serverDeviceName = serverName
                                                        .replaceAll(ISourceDevice.DEFAULT_SEPARATOR, getPathSeparator())
                                                        + getPathSeparator() + deviceName.toLowerCase();
                                                if (!serverList.contains(serverDeviceName)) {
                                                    serverList.add(serverDeviceName);
                                                    // System.out.println("server "+ serverDeviceName);
                                                }

                                                if (deviceList == null) {
                                                    deviceList = new ArrayList<>();
                                                    SERVER_MAP.put(serverName.toLowerCase(), deviceList);
                                                }

                                                if (!deviceList.contains(deviceName.toLowerCase())) {
                                                    deviceList.add(deviceName.toLowerCase());
                                                }
                                            } // End if device exist
                                        } // End for device list
                                    } // End if
                                } catch (DevFailed devFailed) {
                                    controller.logError(this.getClass().getName(),
                                            new Exception(DevFailedUtils.toString(devFailed)));
                                }
                            } // End If server not created

                            int newPercent = (i++) * 100 / nbSource;
                            newPercent = (newPercent * 30) / 100;
                            if (newPercent > percent) {
                                percent = newPercent;
                                controller.increaseLoadingProgress(1);
                            }
                        } // End for server list

                    } // End if server list not null
                } // End if database is not null
            } // End if sourceList not null
            Collections.sort(serverList, Collator.getInstance());
        }
        return serverList;
    }

    @Override
    public String getName() {
        return "Servers";
    }

    @Override
    public String getInformation() {
        return "Servers tree";
    }

    @Override
    public String getPathSeparator() {
        return SEPARATOR;
    }

    public List<String> getRelatedDevices(final String server) {
        List<String> relatedDevices = null;
        if (server != null) {
            relatedDevices = SERVER_MAP.get(server.toLowerCase());
            if (serverList == null && relatedDevices == null) {
                String title = "Load server " + server;
                controller.showWaitingDialog(null, title);
                Thread runnable = new Thread(title) {
                    @Override
                    public void run() {
                        try {
                            getSourceList();
                        } catch (SourceDeviceException ex) {
                            controller.logMessage(this.getClass().getSimpleName(), ex.getMessage());
                        }
                        controller.hideWaitingDialog();
                    };
                };
                runnable.start();
            }
            relatedDevices = SERVER_MAP.get(server.toLowerCase());
        }
        return relatedDevices;
    }

    public List<String> getDeviceListForServer(final String server) {
        List<String> deviceList = new ArrayList<>();
        Set<String> keySet = SERVER_MAP.keySet();
        if (server != null && !server.isEmpty()) {
            List<String> list = null;
            for (String serverName : keySet) {
                if (controller.isRootNode(serverName, server)) {
                    list = SERVER_MAP.get(serverName);
                    if (list != null) {
                        for (String deviceName : list) {
                            if (!deviceList.contains(deviceName)) {
                                deviceList.add(deviceName);
                            }
                        }
                    }
                }
            }
        }
        return deviceList;
    }

}
