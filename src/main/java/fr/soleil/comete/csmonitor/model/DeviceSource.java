package fr.soleil.comete.csmonitor.model;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.Database;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.DatabaseTangoSourceDevice;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class DeviceSource extends DatabaseTangoSourceDevice {

    private List<String> sourceList = null;
    private List<String> aliasSourceList = null;

    private ICSMonitor controller = null;
    private String argDeviceName = null;

    public DeviceSource(ICSMonitor controller) {
        this.controller = controller;
    }

    public DeviceSource(ICSMonitor controller, String argDeviceName) {
        this.controller = controller;
        this.argDeviceName = argDeviceName;
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        if (argDeviceName != null && sourceList == null) {
            sourceList = new ArrayList<String>();
            aliasSourceList = new ArrayList<String>();
            boolean isAlias = false;
            String sourceAlias = null;
            Database database = null;
            sourceList.add(argDeviceName.toLowerCase());
            controller.setLoadingMessage("device = " + argDeviceName);
            List<String> allSource = super.getSourceList();

            for (String source : allSource) {
                String sourceLowerCase = source.toLowerCase();
                if (!isAlias) {
                    try {
                        if (database == null) {
                            database = TangoDeviceHelper.getDatabase();
                        }
                        String alias = database.get_alias_from_device(source);
                        aliasSourceList.add(alias.toLowerCase());
                    } catch (DevFailed e) {
                        // Device don't contains alias to do nothing
                    }

                    if (aliasSourceList.contains(argDeviceName)) {
                        sourceAlias = source;
                        isAlias = true;
                        break;
                    }

                    if (source.contains(argDeviceName) || sourceLowerCase.contains(argDeviceName)) {
                        sourceList.add(source.toLowerCase());
                    }
                }
            }

            if (isAlias && (sourceAlias != null)) {
                sourceList.add(sourceAlias);
            }
        } else {
            if (sourceList == null) {
                sourceList = new ArrayList<String>();

                List<String> originalSourceList = super.getSourceList();
                if (originalSourceList != null && !originalSourceList.isEmpty()) {
                    int nbSource = originalSourceList.size();
                    int i = 0;
                    int percent = 0;

                    for (String source : originalSourceList) {
                        controller.setLoadingMessage("Loading device " + source);
                        if (!excludeDevice(source)) {
                            sourceList.add(source.toLowerCase());
                        }

                        int newPercent = (i++) * 100 / nbSource;
                        newPercent = (newPercent * 60) / 100;
                        if (newPercent > percent) {
                            percent = newPercent;
                            controller.increaseLoadingProgress(1);
                        }
                    }
                }
                Collections.sort(sourceList, Collator.getInstance());
            }
        }
        return sourceList;
    }

    private boolean excludeDevice(String device) {
        boolean exclude = false;
        try {
            Database database = TangoDeviceHelper.getDatabase();
            String get_class_for_device = database.get_class_for_device(device);
            if (get_class_for_device == null || get_class_for_device.isEmpty()
                    || get_class_for_device.equalsIgnoreCase("TangoUnit")
                    || get_class_for_device.equalsIgnoreCase("DServer") || device.contains("@") || device.contains("#")
                    || device.contains("CompactPCICrate")) {
                exclude = true;
            }
        } catch (DevFailed devFailed) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(devFailed)));
            exclude = true;
        }

        return exclude;
    }

    @Override
    public String getInformation() {
        return super.getInformation();
    }

}
