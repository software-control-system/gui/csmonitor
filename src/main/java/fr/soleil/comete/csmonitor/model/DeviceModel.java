package fr.soleil.comete.csmonitor.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.CurrentScanDataModel;

public class DeviceModel {

    private static final Map<String, DeviceModel> DEVICE_MAP = new ConcurrentHashMap<>();
    private static final Map<String, List<String>> SUB_DEVICE_MAP = new ConcurrentHashMap<>();
    private static final Collection<String> DEVICE_LIST = new LinkedHashSet<>();
    private static final RefresherState REFRESHER_STATE = new RefresherState();
    // Starter Map <Host, StarterDeviceName>
    private static final Map<String, String> STARTER_MAP = new ConcurrentHashMap<>();
    private static final Collection<String> HOST_LIST = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private static final Map<String, String> HOST_NAME_MAP = new ConcurrentHashMap<>();
    private static ICSMonitor controller;

    private final String deviceName;
    private DevState state;
    private DeviceInfo deviceInfo;
    private String starterDeviceName;

    public DeviceModel(final String deviceName, final ICSMonitor controller) {
        state = DevState.UNKNOWN;
        this.deviceName = deviceName;
        DeviceModel.controller = controller;
        refreshState();
    }

    public boolean isStartable() {
        getStarterDeviceName();
        return starterDeviceName != null && !starterDeviceName.isEmpty();
    }

    public String getStarterDeviceName() {
        if (starterDeviceName == null) {
            DeviceInfo devInf = getDeviceInfo();
            if (devInf != null && devInf.hostname != null) {
                String hostName = devInf.hostname;
                if (!hostName.isEmpty()) {
                    starterDeviceName = STARTER_MAP.get(hostName.toLowerCase());
                    if (starterDeviceName == null) {
                        Database database = TangoDeviceHelper.getDatabase();
                        if (database != null) {
                            try {
                                String shortHost = getShortHostName(hostName);
                                String starterServerName = "starter/" + shortHost + "*";
                                String[] device_name_list = database.get_device_name(starterServerName, "starter");
                                if (device_name_list != null && device_name_list.length > 0
                                        && !device_name_list[0].isEmpty()) {
                                    starterDeviceName = device_name_list[0];
                                    STARTER_MAP.put(hostName.toLowerCase(), starterDeviceName);
                                }
                            } catch (DevFailed e) {
                                controller.logError(this.getClass().getSimpleName(),
                                        new Exception(TangoExceptionHelper.getErrorMessage(e), e));
                            }
                        }
                        if (starterDeviceName == null) {
                            STARTER_MAP.put(hostName.toLowerCase(), ObjectUtils.EMPTY_STRING);
                        }
                    }
                }
            }
            if (starterDeviceName == null) {
                starterDeviceName = ObjectUtils.EMPTY_STRING;
            }
        }
        return starterDeviceName;
    }

    private String getShortHostName(String hostName) {
        String shortHost = HOST_NAME_MAP.get(hostName.toLowerCase());
        if (shortHost == null) {
            if (HOST_LIST.isEmpty()) {
                Database database = TangoDeviceHelper.getDatabase();
                if (database != null) {
                    try {
                        String[] host_name_list = database.get_host_list();
                        if (host_name_list != null && host_name_list.length > 0) {
                            for (String host : host_name_list) {
                                HOST_LIST.add(host.toLowerCase());
                            }
                        }
                    } catch (DevFailed e) {
                        controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
                    }
                }
            }

            for (String host : HOST_LIST) {
                if (host != null && !host.isEmpty() && hostName.startsWith(host) && host.length() < hostName.length()) {
                    shortHost = host;
                    break;
                }
            }

            if (shortHost == null || shortHost.isEmpty()) {
                shortHost = hostName.toLowerCase();
            }

            HOST_NAME_MAP.put(hostName.toLowerCase(), shortHost);
        }

        return shortHost;
    }

    public void refreshInfo() {
        deviceInfo = null;
        refreshState();
    }

    public void refreshState() {
        DevState oldstate = state;
        DeviceInfo deviceInfo2 = getDeviceInfo();
        DevState state;
        if (deviceInfo2 == null) {
            state = DevState.UNKNOWN;
        } else if (deviceInfo2.exported) {
            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
            if (deviceProxy == null) {
                state = DevState.UNKNOWN;
                // XXX a client of a device MUST never unexport that device
//                // Unexport device if it s not running
//                Database database = TangoDeviceHelper.getDatabase();
//                try {
//                    if (database != null) {
//                        database.unexport_device(deviceName);
//                    }
//                } catch (DevFailed df) {
//                    state = DevState.UNKNOWN;
//                }
            } else {
                try {
                    state = deviceProxy.state();
                } catch (DevFailed df) {
                    state = DevState.UNKNOWN;
                }
            }
        } else {
            state = DevState.UNKNOWN;
        }
        this.state = state;
        if (!ObjectUtils.sameObject(oldstate, state)) {
            CSEvent event = new CSEvent(DeviceModel.class.getName(), deviceName, EventType.STATE_REFRESHED);
            controller.fireSystemControlChanged(event);
        }
    }

    public DevState getState() {
        // call refresh state in a separated thread
        if ((deviceName != null) && !deviceName.isEmpty()) {
            synchronized (DEVICE_LIST) {
                DEVICE_LIST.add(deviceName);
            }
            if (!REFRESHER_STATE.isAlive()) {
                try {
                    REFRESHER_STATE.start();
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
        return state;
    }

    public DeviceInfo getDeviceInfo() {
        if (deviceInfo == null) {
            try {
                Database database = TangoDeviceHelper.getDatabase();
                deviceInfo = database.get_device_info(deviceName);
            } catch (DevFailed e) {
                deviceInfo = null;
            }
        }
        return deviceInfo;
    }

    public static DeviceModel getDeviceModel(final String deviceName, ICSMonitor controller) {
        DeviceModel deviceModel = null;
        if (deviceName != null) {
            deviceModel = DEVICE_MAP.get(deviceName.toLowerCase());
            if (deviceModel == null) {
                deviceModel = new DeviceModel(deviceName.toLowerCase(), controller);
                DEVICE_MAP.put(deviceName.toLowerCase(), deviceModel);
            }
        }
        return deviceModel;
    }

    public List<String> getSubDevice() {
        List<String> subDevices = null;
        if (deviceName != null) {
            subDevices = SUB_DEVICE_MAP.get(deviceName.toLowerCase());
            if (subDevices == null && controller != null) {
                List<String> sourceList = null;
                try {
                    sourceList = controller.getDeviceSource().getSourceList();
                } catch (SourceDeviceException e) {
                    controller.logError(this.getClass().getSimpleName(), e);
                }

                if (sourceList != null) {
                    subDevices = new ArrayList<String>();
                    try {
                        Database database = TangoDeviceHelper.getDatabase();
                        String[] get_device_property_list = database.get_device_property_list(deviceName, "*");
                        if (get_device_property_list != null) {
                            DbDatum property = null;
                            String[] propertyValueList = null;
                            for (String propName : get_device_property_list) {
                                try {
                                    property = database.get_device_property(deviceName, propName);
                                    if (property != null && !property.is_empty()) {
                                        propertyValueList = property.extractStringArray();
                                        if (propertyValueList != null) {
                                            for (String propertyValue : propertyValueList) {
                                                List<String> tmpSubDevices = getDeviceListFromProperty(propertyValue,
                                                        sourceList);
                                                if (tmpSubDevices != null) {
                                                    for (String subDev : tmpSubDevices) {
                                                        if (!subDevices.contains(subDev.toLowerCase())) {
                                                            subDevices.add(subDev.toLowerCase());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } catch (DevFailed devFailed) {
                                    controller.logError(this.getClass().getName(),
                                            new Exception(DevFailedUtils.toString(devFailed)));
                                }
                            }
                        }
                    } catch (DevFailed e) {
                        controller.logError(this.getClass().getName(), new Exception(DevFailedUtils.toString(e)));
                    }
                }
            }
        }

        if (subDevices != null && !subDevices.isEmpty()) {
            SUB_DEVICE_MAP.put(deviceName.toLowerCase(), subDevices);
        }

        if (subDevices == null) {
            subDevices = new ArrayList<String>();
        }

        subDevices = addSpecificSubdevice(subDevices);

        return subDevices;
    }

    private List<String> addSpecificSubdevice(List<String> subDevices) {
        List<String> tmpSubDevices = subDevices;

        DeviceInfo deviceInfo2 = getDeviceInfo();
        if (deviceInfo2 != null && deviceInfo2.exported) {
            Database database = TangoDeviceHelper.getDatabase();
            String className = ObjectUtils.EMPTY_STRING;
            try {
                className = database.get_class_for_device(deviceName);
            } catch (DevFailed e) {
                controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
            }

            try {
                ClassName valueOf = ClassName.valueOf(className);
                if (valueOf != null) {
                    DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(deviceName);
                    switch (valueOf) {
                        case ScanServer:
                            addSpecificSubdeviceForScanServer(tmpSubDevices, deviceProxy);
                            break;
                        case DataFitter:
                            addSpecificSubdeviceForDataFitter(tmpSubDevices, deviceProxy);
                            break;
                        case FlyScanServer:
                            addSpecificSubdeviceForFlyScanServer(tmpSubDevices, deviceProxy);
                            break;

                    }
                }
            }

            catch (Exception e) {
                // It is not a specific class
            }

        }

        return tmpSubDevices;
    }

    private List<String> addSpecificSubdeviceForScanServer(List<String> subDevices, final DeviceProxy deviceProxy) {
        List<String> tmpSubDevices = subDevices;
        if (deviceProxy != null) {
            tmpSubDevices = new ArrayList<String>();
            tmpSubDevices.addAll(subDevices);

            addSubDevicesFromAttribute(tmpSubDevices, deviceProxy, CurrentScanDataModel.ACTUATORS_LIST);
            addSubDevicesFromAttribute(tmpSubDevices, deviceProxy, CurrentScanDataModel.YACTUATORS_LIST);
            addSubDevicesFromAttribute(tmpSubDevices, deviceProxy, CurrentScanDataModel.SENSORS_LIST);

            addSubDevicesFromDevice(tmpSubDevices, deviceProxy, CurrentScanDataModel.TIMEBASES);

            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.PRE_RUN_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.PRE_SCAN_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.PRE_STEP_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.POST_ACTUATOR_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.POST_INTEGRATION_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.POST_STEP_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.POST_SCAN_HOOK);
            addSubDevicesFromCommand(tmpSubDevices, deviceProxy, CurrentScanDataModel.POST_RUN_HOOK);

            // Check context validation
            try {
                DeviceAttribute attribute = deviceProxy.read_attribute(CurrentScanDataModel.CONTEXT_VALIDATION);
                String extractString = attribute.extractString();
                addSubDeviceFromAttribute(tmpSubDevices, extractString);
            } catch (DevFailed e) {
                controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
            }
        }
        return tmpSubDevices;
    }

    private List<String> addSpecificSubdeviceForDataFitter(List<String> subDevices, final DeviceProxy deviceProxy) {
        List<String> tmpSubDevices = subDevices;
        if (deviceProxy != null) {
            tmpSubDevices = new ArrayList<String>();
            tmpSubDevices.addAll(subDevices);
            addSubDeviceFromAttribute(tmpSubDevices, deviceProxy, "deviceAttributeNameSigma");
            addSubDeviceFromAttribute(tmpSubDevices, deviceProxy, "deviceAttributeNameX");
            addSubDeviceFromAttribute(tmpSubDevices, deviceProxy, "deviceAttributeNameY");
        }
        return tmpSubDevices;
    }

    private List<String> addSpecificSubdeviceForFlyScanServer(List<String> subDevices, final DeviceProxy deviceProxy) {
        List<String> tmpSubDevices = subDevices;
        if (deviceProxy != null) {
            tmpSubDevices = new ArrayList<String>();
            tmpSubDevices.addAll(subDevices);
            addSubDevicesFromAttribute(tmpSubDevices, deviceProxy, "monitoredDeviceNames");
        }
        return tmpSubDevices;
    }

    private String readAttribute(final DeviceProxy deviceProxy, final String attributeName) {
        String extractString = null;
        try {
            DeviceAttribute attribute = deviceProxy.read_attribute(attributeName);
            extractString = attribute.extractString();
        } catch (DevFailed e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
        }
        return extractString;
    }

    private String[] readAttributeArray(final DeviceProxy deviceProxy, final String attributeName) {
        String[] extractStringArray = null;
        try {
            DeviceAttribute attribute = deviceProxy.read_attribute(attributeName);
            extractStringArray = attribute.extractStringArray();
        } catch (DevFailed e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
        }
        return extractStringArray;
    }

    private void addSubDeviceFromAttribute(final List<String> subDevices, final DeviceProxy deviceProxy,
            final String attributeName) {
        String extractString = readAttribute(deviceProxy, attributeName);
        addSubDeviceFromAttribute(subDevices, extractString);
    }

    private void addSubDevicesFromAttribute(final List<String> subDevices, final DeviceProxy deviceProxy,
            final String attributeName) {

        String[] extractStringArray = readAttributeArray(deviceProxy, attributeName);
        if (extractStringArray != null && extractStringArray.length > 0) {
            for (String device : extractStringArray) {
                addSubDeviceFromAttribute(subDevices, device);
            }
        }

    }

    private void addSubDevicesFromCommand(final List<String> subDevices, final DeviceProxy deviceProxy,
            final String attributeName) {
        String[] extractStringArray = readAttributeArray(deviceProxy, attributeName);
        if (extractStringArray != null && extractStringArray.length > 0) {
            for (String device : extractStringArray) {
                addSubDeviceFromCommand(subDevices, device);
            }
        }

    }

    private void addSubDevicesFromDevice(final List<String> subDevices, final DeviceProxy deviceProxy,
            final String attributeName) {
        String[] extractStringArray = readAttributeArray(deviceProxy, attributeName);
        if (extractStringArray != null && extractStringArray.length > 0) {
            for (String device : extractStringArray) {
                addSubDeviceFromDevice(subDevices, device);
            }
        }
    }

    private void addSubDeviceFromDevice(final List<String> subDevices, String deviceName) {
        try {
            if (deviceName != null && !deviceName.isEmpty()) {
                String devName = TangoUtil.getfullNameForDevice(deviceName);
                addSubDevice(subDevices, devName);
            }
        } catch (DevFailed e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
        } catch (Exception e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(e.getMessage()));
        }
    }

    private void addSubDeviceFromAttribute(final List<String> subDevices, String attributeName) {
        try {
            if (attributeName != null && !attributeName.isEmpty() && attributeName.contains("/")) {
                String devName = TangoUtil.getfullDeviceNameForAttribute(attributeName);
                addSubDevice(subDevices, devName);
            }
        } catch (DevFailed e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
        } catch (Exception e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(e.getMessage()));
        }
    }

    private void addSubDeviceFromCommand(final List<String> subDevices, String commandName) {
        try {
            if (commandName != null && !commandName.isEmpty() && commandName.contains("/")) {
                String devName = TangoUtil.getFullDeviceNameForCommand(commandName);
                addSubDevice(subDevices, devName);
            }
        } catch (DevFailed e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
        } catch (Exception e) {
            controller.logError(this.getClass().getSimpleName(), new Exception(e.getMessage()));
        }
    }

    private void addSubDevice(final List<String> deviceList, String deviceName) {
        if (deviceName != null && !deviceName.isEmpty() && !deviceList.contains(deviceName.toLowerCase())) {
            deviceList.add(deviceName.toLowerCase());
        }
    }

    private List<String> getDeviceListFromProperty(String propValue, List<String> deviceNameList) {
        List<String> deviceList = null;
        if (propValue != null && !propValue.isEmpty()) {
            deviceList = new ArrayList<String>();
            for (String deviceName : deviceNameList) {
                if (propValue.toLowerCase().contains(deviceName.toLowerCase())) {
                    addSubDevice(deviceList, deviceName);
                }
            }
        }

        return deviceList;
    }

    public List<String> getRelatedDevice() {
        List<String> relatedDevice = null;
        DeviceInfo deviceInfo2 = getDeviceInfo();
        if (deviceInfo2 != null) {
            ISourceDevice serverSource = controller.getServerSource();
            if (serverSource instanceof ServerSource) {
                List<String> tmpRelatedDevice = ((ServerSource) serverSource).getRelatedDevices(deviceInfo2.server);
                if (tmpRelatedDevice != null) {
                    relatedDevice = new ArrayList<String>(tmpRelatedDevice);
                    if (relatedDevice.contains(deviceName.toLowerCase())) {
                        relatedDevice.remove(deviceName.toLowerCase());
                    }
                }
            }
        }
        return relatedDevice;
    }

    public static void refreshAllInfo() {
        Collection<DeviceModel> values = DEVICE_MAP.values();
        for (DeviceModel deviInfo : values) {
            deviInfo.refreshInfo();
            deviInfo.refreshState();
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    private static enum ClassName {
        ScanServer, DataFitter, FlyScanServer
    }

    private static class RefresherState extends Thread {

        public RefresherState() {
            super(RefresherState.class.getSimpleName());
        }

        @Override
        public void run() {
            while (true) {
                try {
                    List<String> tmpDeviceList;
                    synchronized (DEVICE_LIST) {
                        if (DEVICE_LIST.isEmpty()) {
                            tmpDeviceList = null;
                        } else {
                            tmpDeviceList = new LinkedList<>(DEVICE_LIST);
                        }
                    }
                    if (tmpDeviceList != null) {
                        while (!tmpDeviceList.isEmpty()) {
                            String devName = tmpDeviceList.remove(0);
                            DeviceModel deviceModel = getDeviceModel(devName, controller);
                            if (deviceModel != null) {
                                deviceModel.refreshState();
                            }
                        }
                    }
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    controller.logError(RefresherState.class.getSimpleName(), e);
                }
            }
        }
    }

}
