package fr.soleil.comete.csmonitor.view;

import fr.soleil.comete.swing.FileBrowserFieldButton;

public class FileBrowserField extends FileBrowserFieldButton {

    private static final long serialVersionUID = 6222204317710224935L;

    public FileBrowserField() {
        super();
    }

    @Override
    protected void initListeners() {
        super.initListeners();
        textField.addActionListener(textField);
    }

    public void setFieldText(String text) {
        textField.cancel();
        textField.setText(text);
    }

    @Override
    public void setText(String text) {
        setFieldText(text);
    }

}
