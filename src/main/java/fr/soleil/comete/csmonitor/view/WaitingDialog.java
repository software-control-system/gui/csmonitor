package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

public class WaitingDialog extends JDialog {

    private static final long serialVersionUID = 2282544494179147043L;

    private JProgressBar progressBar;

    public WaitingDialog(final Component component) {
        super(SwingUtilities.getWindowAncestor(component));
        setTitle("Please wait ...");
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setSize(500, 60);
        setPreferredSize(getSize());

        initComponents();
        layoutComponents();

        pack();
        setResizable(false);
        setLocationRelativeTo(component);
    }

    private void initComponents() {
        progressBar = new JProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);
        progressBar.setString("");
    }

    private void layoutComponents() {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(progressBar, BorderLayout.CENTER);
        setContentPane(contentPane);
    }

    public void setMainMessage(final String message) {
        progressBar.setString(message);
    }

    public void clear() {
        progressBar.setString("");
    }

}
