package fr.soleil.comete.csmonitor.view;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.prefs.BackingStoreException;
import java.util.prefs.InvalidPreferencesFormatException;
import java.util.prefs.Preferences;

import fr.soleil.lib.project.SystemUtils;

public class PreferenceFile {

    private static final boolean IS_WINDOWS = System.getProperty("os.name").contains("Windows");

    private static final String WINDOW_BOUNDS_X = "x";
    private static final String WINDOW_BOUNDS_Y = "y";
    private static final String WINDOW_BOUNDS_WIDTH = "width";
    private static final String WINDOW_BOUNDS_HEIGHT = "height";

    private static final String TREE_DIVIDER_LOCATION = "TreeDividerLocation";
    private static final String LOG_DIVIDER_LOCATION = "LogDividerLocation";
    
    private static final int DEFAULT_TREE_LOCATION = 200;
    private static final int DEFAULT_LOG_LOCATION = 750;
    
    private static final String NORTH_DIVIDER_LOCATION = "DeviceStateAndInformationTableDividerLocation";
    private static final String CENTER_DIVIDER_LOCATION = "InformationTableAndSubDeviceTableDividerLocation";
    private static final String SOUTH_DIVIDER_LOCATION = "SubAndRelatedTableDividerLocation";

    private static final int DEFAULT_CENTER_LOCATION = 450;
    private static final int DEFAULT_NORTH_SOUTH_LOCATION = 200;
    
    private static final String CONFIRMATION_DIALOG_DIVIDER_LOCATION = "AskConfirmationDialog";
    private static final boolean DEFAULT_CONFIRMATION_DIALOG = true;
    
    private static final String SHOW_SUBDEVICES_PANEL = "ShowSubdevicesPanel";
    private static final boolean DEFAULT_SHOW_SUBDEVICESPANEL = false;

    private static final String SYSTEMCONTROLMONITOR_NODE = "CSMonitor";
    private static final String USER_NODE = "UserPreferences";
    private static final String SAVE_LOCATION_PATH = "SaveLocationPath";

    private static final String CONTROL_LOCATION = "controlLocation";
    private static final String JIVE_LOCATION = "jiveLocation";
    private static final String ASTOR_LOCATION = "astorLocation";

    private static final String CONFIGURATION_FILE_NAME = "csmonitorconfig";
    private static final String CONFIGURATION_FILE_EXT = "xml";

    private static final String FOLDER_NAME = "CSMonitor";

    private static final String DEFAULT_CONTROL_PANEL = IS_WINDOWS ? "devicepanel.bat" : "devicepanel";
    private static final String DEFAULT_JIVE = IS_WINDOWS ? "jive-rw.bat" : "jive-rw";
    private static final String DEFAULT_ASTOR = IS_WINDOWS ? "astor.bat" : "astor";

    private static String controlPanel = DEFAULT_CONTROL_PANEL;
    private static String jive = DEFAULT_JIVE;
    private static String astor = DEFAULT_ASTOR;

    private static String userDirectory;

    static {
        loadSettings();
    }

    public static Rectangle getWindowBounds() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle maximumWindowBounds = ge.getMaximumWindowBounds();
        int x = maximumWindowBounds.x;
        int y = maximumWindowBounds.y;
        int w = maximumWindowBounds.width;
        int h = maximumWindowBounds.height;

        Rectangle result = new Rectangle(x, y, w, h);

        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    result.x = window.getInt(WINDOW_BOUNDS_X, x);
                    result.y = window.getInt(WINDOW_BOUNDS_Y, y);
                    result.width = window.getInt(WINDOW_BOUNDS_WIDTH, w - result.x);
                    result.height = window.getInt(WINDOW_BOUNDS_HEIGHT, h - result.y);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public static void setWindowBounds(final Rectangle windowBounds) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(WINDOW_BOUNDS_X, windowBounds.x);
                window.putInt(WINDOW_BOUNDS_Y, windowBounds.y);
                window.putInt(WINDOW_BOUNDS_WIDTH, windowBounds.width);
                window.putInt(WINDOW_BOUNDS_HEIGHT, windowBounds.height);
            }
        }
    }

    public static void setTreeDividerLocation(int location) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(TREE_DIVIDER_LOCATION, location);
            }
        }
    }

    public static int getTreeDividerLocation() {
        int defaultValue = DEFAULT_TREE_LOCATION;
        int location = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    location = window.getInt(TREE_DIVIDER_LOCATION, defaultValue);
                } catch (Exception e) {
                    location = defaultValue;
                }
            }
        }
        return location;
    }

    public static void setLogDividerLocation(int location) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(LOG_DIVIDER_LOCATION, location);
            }
        }
    }

    public static int getLogDividerLocation() {
        int defaultValue = DEFAULT_LOG_LOCATION;
        int location = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    location = window.getInt(LOG_DIVIDER_LOCATION, defaultValue);
                } catch (Exception e) {
                    location = defaultValue;
                }
            }
        }
        return location;
    }
    
    public static void setNorthDividerLocation(int location) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(NORTH_DIVIDER_LOCATION, location);
            }
        }
    }

    public static int getNorthDividerLocation() {
        int defaultValue = DEFAULT_NORTH_SOUTH_LOCATION;
        int location = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    location = window.getInt(NORTH_DIVIDER_LOCATION, defaultValue);
                } catch (Exception e) {
                    location = defaultValue;
                }
            }
        }
        return location;
    }
    
    public static void setSouthDividerLocation(int location) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(SOUTH_DIVIDER_LOCATION, location);
            }
        }
    }

    public static int getSouthDividerLocation() {
        int defaultValue = DEFAULT_NORTH_SOUTH_LOCATION;
        int location = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    location = window.getInt(SOUTH_DIVIDER_LOCATION, defaultValue);
                } catch (Exception e) {
                    location = defaultValue;
                }
            }
        }
        return location;
    }
    
    public static void setCenterDividerLocation(int location) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putInt(CENTER_DIVIDER_LOCATION, location);
            }
        }
    }

    public static int getCenterDividerLocation() {
        int defaultValue = DEFAULT_CENTER_LOCATION;
        int location = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    location = window.getInt(CENTER_DIVIDER_LOCATION, defaultValue);
                } catch (Exception e) {
                    location = defaultValue;
                }
            }
        }
        return location;
    }

    public static void setAskDialogConfirmation(boolean dialogConfirmation) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putBoolean(CONFIRMATION_DIALOG_DIVIDER_LOCATION, dialogConfirmation);
            }
        }
    }

    public static boolean getAskDialogConfirmationValue() {
        boolean defaultValue = DEFAULT_CONFIRMATION_DIALOG;
        boolean confirmationDialog = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    confirmationDialog = window.getBoolean(CONFIRMATION_DIALOG_DIVIDER_LOCATION, confirmationDialog);
                } catch (Exception e) {
                    confirmationDialog = defaultValue;
                }
            }
        }
        return confirmationDialog;
    }
    
    public static void setShowSubdevicesPanel(boolean showSubdevicesPanel) {
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                window.putBoolean(SHOW_SUBDEVICES_PANEL, showSubdevicesPanel);
            }
        }
    }

    public static boolean getShowSubdevicesPanel() {
        boolean defaultValue = DEFAULT_SHOW_SUBDEVICESPANEL;
        boolean showSubdevicesPanel = defaultValue;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences window = applicationNode.node(USER_NODE);
            if (window != null) {
                try {
                    showSubdevicesPanel = window.getBoolean(SHOW_SUBDEVICES_PANEL, showSubdevicesPanel);
                } catch (Exception e) {
                    showSubdevicesPanel = defaultValue;
                }
            }
        }
        return showSubdevicesPanel;
    }

    public static String getUserDirectory() {
        if (userDirectory == null || userDirectory.isEmpty()) {
            // TODO maybe use System.getProperty("user.home")
            String homePath = SystemUtils.getSystemProperty("HOME");
            if (homePath == null || homePath.isEmpty()) {
                try {
                    File createTempFile = File.createTempFile(CONFIGURATION_FILE_NAME, CONFIGURATION_FILE_EXT);
                    homePath = createTempFile.getParent();
                    createTempFile.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (homePath != null && !homePath.isEmpty()) {
                userDirectory = homePath + File.separator + FOLDER_NAME;
                File createFolder = new File(homePath + FOLDER_NAME);
                createFolder.mkdirs();
            }
        }
        return userDirectory;
    }

    private static String getConfigurationFileName() {
        String configurationFileName = null;
        String directory = getUserDirectory();
        if (directory != null && !directory.isEmpty()) {
            configurationFileName = directory + File.separator + CONFIGURATION_FILE_NAME + "." + CONFIGURATION_FILE_EXT;
        }
        return configurationFileName;
    }

    public static String getSaveLocationPath() {
        String saveLocationPath = getUserDirectory();
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNodes = applicationNode.node(USER_NODE);
            if (userNodes != null) {
                try {
                    saveLocationPath = userNodes.get(SAVE_LOCATION_PATH, getUserDirectory());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return saveLocationPath;
    }

    public static void setSaveLocationPath(String location) {
        if (location != null) {
            Preferences applicationNode = getUserPreferences();
            if (applicationNode != null) {
                Preferences userNodes = applicationNode.node(USER_NODE);
                if (userNodes != null) {
                    try {
                        userNodes.put(SAVE_LOCATION_PATH, location);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static Preferences getUserPreferences() {
        Preferences applicationNode = null;
        try {
            applicationNode = Preferences.userRoot().node(SYSTEMCONTROLMONITOR_NODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return applicationNode;
    }

    // DevicePanel
    public static String getControlPanelPath() {
        String result = controlPanel;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if (userNode != null) {
                result = userNode.get(CONTROL_LOCATION, controlPanel);
                controlPanel = result;
            }
        }
        return result;
    }

    public static void setControlPanelPath(final String path) {
        controlPanel = path;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if ((userNode != null) && (path != null)) {
                userNode.put(CONTROL_LOCATION, path);
            }
        }
    }

    // Jive
    public static String getJivePath() {
        String result = jive;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if (userNode != null) {
                result = userNode.get(JIVE_LOCATION, jive);
                jive = result;
            }
        }
        return result;
    }

    public static void setJivePath(final String path) {
        jive = path;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if ((userNode != null) && (path != null)) {
                userNode.put(JIVE_LOCATION, path);
            }
        }
    }

    // Astor
    public static String getAstorPath() {
        String result = astor;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if (userNode != null) {
                result = userNode.get(ASTOR_LOCATION, astor);
                astor = result;
            }
        }
        return result;
    }

    public static void setAstorPath(final String path) {
        astor = path;
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            Preferences userNode = applicationNode.node(USER_NODE);
            if ((userNode != null) && (path != null)) {
                userNode.put(ASTOR_LOCATION, path);
            }
        }
    }

    // Save and load settings

    public static void saveSettings() {
        Preferences applicationNode = getUserPreferences();
        String configFilename = getConfigurationFileName();
        if (applicationNode != null) {
            OutputStream out = null;
            try {
                applicationNode.flush();
                out = new FileOutputStream(configFilename);
                applicationNode.exportSubtree(out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void loadSettings() {
        Preferences applicationNode = getUserPreferences();

        if (applicationNode != null) {
            try {
                applicationNode.removeNode();
            } catch (BackingStoreException e1) {
                e1.printStackTrace();
            }
            String configFilename = getConfigurationFileName();
            if (checkFileCreation(configFilename)) {
                File tmp = new File(configFilename);
                // When file is empty, Preferences.importPreferences will throw an Exception
                if (tmp.length() > 0) {
                    InputStream in = null;
                    try {
                        in = new FileInputStream(configFilename);
                        Preferences.importPreferences(in);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InvalidPreferencesFormatException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (in != null) {
                                in.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            // LOGGER.error("Impossible to load settings");
            System.out.println("Impossible to load settings");
        }
    }

    private static boolean checkFileCreation(String configFilename) {
        boolean fileIsCreated = false;
        if (configFilename != null && !configFilename.isEmpty()) {
            File file = new File(configFilename);
            try {
                if (!file.exists()) {
                    File parentFile = file.getParentFile();
                    if (!parentFile.exists()) {
                        parentFile.mkdirs();
                    }
                    file.createNewFile();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileIsCreated = file.exists();
        }
        return fileIsCreated;
    }

    public static void main(String[] args) throws Exception {
//      PreferenceFile.loadSettings();
//      PreferenceFile.saveSettings();
//      PreferenceFile.getUserDirectory();
        Preferences applicationNode = getUserPreferences();
        if (applicationNode != null) {
            applicationNode.clear();
            applicationNode.removeNode();
        }
        PreferenceFile.setControlPanelPath(DEFAULT_CONTROL_PANEL);
        PreferenceFile.setJivePath(DEFAULT_JIVE);
        PreferenceFile.setAstorPath(DEFAULT_ASTOR);
        PreferenceFile.saveSettings();
//        String controlPanel2 = getControlPanel();
        System.exit(0);
    }

}
