package fr.soleil.comete.csmonitor.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import fr.esrf.Tango.DevState;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.lib.project.file.BatchExecutor;

public class DevicePopupMenu extends JPopupMenu {

    private static final long serialVersionUID = 6066112379857244700L;

    private JMenuItem menuMonitorDevice;
    private JMenuItem menuStart;
    private JMenuItem menuStop;
    private JMenuItem menuRepair;
    private JMenuItem menuInit;
    private JMenuItem menuRefreshState;
    private JMenuItem menuGoToDeviceNode;
    private JMenuItem menuGoToServerNode;

    private BatchExecutor controlBatchExecutor;
    private Collection<String> deviceList;
    private ICSMonitor controller;

    public DevicePopupMenu(final ICSMonitor controller) {
        super();
        this.controller = controller;

        menuMonitorDevice = new JMenuItem("Monitor device");
        ImageIcon monitorIcon = ResourcesUtil.getIcon("CSMonitor.MonitorIcon");
        menuMonitorDevice.setIcon(monitorIcon);

        menuRepair = new JMenuItem("Repair device");
        ImageIcon repairIcon = ResourcesUtil.getIcon("CSMonitor.RepairIcon");
        menuRepair.setIcon(repairIcon);

        menuStart = new JMenuItem("Start Server");
        ImageIcon startIcon = ResourcesUtil.getIcon("CSMonitor.StartIcon");
        menuStart.setIcon(startIcon);

        menuStop = new JMenuItem("Stop Server");
        ImageIcon stopIcon = ResourcesUtil.getIcon("CSMonitor.StopIcon");
        menuStop.setIcon(stopIcon);

        menuInit = new JMenuItem("Init");
        menuRefreshState = new JMenuItem("Refresh State");

        ImageIcon goToIcon = ResourcesUtil.getIcon("CSMonitor.GoTo");

        menuGoToDeviceNode = new JMenuItem("Go to device node");
        menuGoToDeviceNode.setIcon(goToIcon);

        menuGoToServerNode = new JMenuItem("Go to server node");
        menuGoToServerNode.setIcon(goToIcon);

        add(menuMonitorDevice);
        add(menuInit);
        ImageIcon refreshIcon = ResourcesUtil.getIcon("CSMonitor.RefreshIcon");
        menuRefreshState.setIcon(refreshIcon);
        add(menuRefreshState);
        addSeparator();
        add(menuStart);
        add(menuStop);
        add(menuRepair);
        addSeparator();
        add(menuGoToDeviceNode);
        add(menuGoToServerNode);

        menuMonitorDevice.addActionListener((e) -> {
            lauchMonitorDevice();
        });

        menuStart.addActionListener((e) -> {
            controller.startDevice(deviceList);
        });

        menuInit.addActionListener((e) -> {
            boolean askDialogConfirmationValue = PreferenceFile.getAskDialogConfirmationValue();
            if (askDialogConfirmationValue) {
                int option = JOptionPane.showConfirmDialog(null, "Are you sure ?", "Execute init command",
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (option == JOptionPane.YES_OPTION) {
                    controller.initDevice(deviceList);
                }
            } else {
                controller.initDevice(deviceList);
            }
        });

        menuStop.addActionListener((e) -> {
            controller.stopDevice(deviceList);
        });

        menuRepair.addActionListener((e) -> {
            controller.repairDevice(deviceList, null);
        });

        menuRefreshState.addActionListener((e) -> {
            controller.refreshDevice(deviceList);
        });

        menuGoToDeviceNode.addActionListener((e) -> {
            String deviceName = ResourcesUtil.getFirstElement(deviceList);
            controller.setDeviceSelected(deviceName, true);
        });

        menuGoToServerNode.addActionListener((e) -> {
            String deviceName = ResourcesUtil.getFirstElement(deviceList);
            controller.setDeviceSelected(deviceName, false);
        });
    }

    public void setSelectedDevice(Collection<String> deviceList) {
        this.deviceList = deviceList;
    }

    public void setSelectedDevice(String deviceName) {
        this.deviceList = new ArrayList<>();
        deviceList.add(deviceName);
    }

    public void popupMenuEnable(boolean goToDeviceEnable, boolean goToServerEnable) {
        menuInit.setEnabled(false);
        menuStart.setEnabled(false);
        menuStop.setEnabled(false);
        menuRepair.setEnabled(false);
        menuMonitorDevice.setEnabled(false);
        boolean oneSelected = deviceList != null && deviceList.size() == 1;
        menuGoToDeviceNode.setEnabled(goToDeviceEnable && oneSelected);
        menuGoToServerNode.setEnabled(goToServerEnable && oneSelected);

        if (deviceList != null && !deviceList.isEmpty()) {
            if (oneSelected) {
                String clickedDeviceName = ResourcesUtil.getFirstElement(deviceList);
                DeviceModel deviceModel = controller.getDeviceModel(clickedDeviceName);
                if (deviceModel != null) {
                    DevState deviceState = deviceModel.getState();
                    if (!DevState.UNKNOWN.equals(deviceState)) {
                        menuMonitorDevice.setEnabled(true);
                    }
                }
            }

            boolean oneDeviceStartable = controller.isOneDeviceStartable(deviceList);
            boolean isOneDeviceFault = false;
            menuStart.setEnabled(oneDeviceStartable);
            if (!oneDeviceStartable) {
                isOneDeviceFault = controller.isOneDeviceFault(deviceList);
            }
            menuRepair.setEnabled(isOneDeviceFault || oneDeviceStartable);
            boolean oneDeviceRunning = controller.isOneDeviceRunning(deviceList);
            menuInit.setEnabled(oneDeviceRunning);
            menuStop.setEnabled(oneDeviceRunning);

        }
    }

    // Launch Monitor Device with popup menu
    public void lauchMonitorDevice() {
        if (deviceList != null) {

            if (controlBatchExecutor == null) {
                controlBatchExecutor = new BatchExecutor();
            }

            String controlPanel = PreferenceFile.getControlPanelPath();
            String clickedDeviceName = ResourcesUtil.getFirstElement(deviceList);

            if (clickedDeviceName != null && !clickedDeviceName.isEmpty() && controlPanel != null
                    && !controlPanel.isEmpty()) {
                DeviceModel deviceModel = controller.getDeviceModel(clickedDeviceName);
                DevState deviceState = deviceModel.getState();
                if (!DevState.UNKNOWN.equals(deviceState)) {
                    controlBatchExecutor.setBatch(controlPanel);
                    List<String> param = new ArrayList<>();
                    param.add(clickedDeviceName);
                    controlBatchExecutor.setBatchParameters(param);
                    controlBatchExecutor.execute();
                }
            }
        }
    }

}
