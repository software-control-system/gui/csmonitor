package fr.soleil.comete.csmonitor.view;

import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.esrf.TangoApi.DeviceInfo;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.bean.tangotree.swing.SwingDeviceSelector;
import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Tree;
import fr.soleil.lib.project.ObjectUtils;

public class DeviceTreePanel extends JTabbedPane implements ICSMonitorListener {

    private static final long serialVersionUID = 787499916386176971L;

    private static final Collection<String> SELECTED_DEVICE_NAME = new LinkedHashSet<>();

    private ICSMonitor controller;

    private DevicePopupMenu popupMenu;
    private JPopupMenu refreshpopupMenu;
    private JMenuItem menuRefreshAll;

    private Tree deviceTree;
    private Tree serverTree;
    private JScrollPane devicePane;
    private JScrollPane serverPane;

    private String oldKeyPress;
    private int countPressKey;
    private boolean firstPress;

    public DeviceTreePanel(ICSMonitor controller, DevicePopupMenu popupMenu) {
        super();
        firstPress = true;
        this.controller = controller;
        this.popupMenu = popupMenu;
        controller.setLoadingMessage(getClass().getName() + " creation...");
        controller.addCSMonitorListener(this);
        controller.increaseLoadingProgress(1);
        initComponent();
    }

    private void initComponent() {
        controller.setLoadingMessage("Create tree devices");
        controller.setLoadingMessage(getClass().getName() + " device source creation...");

        MouseAdapter mouseListener = new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                onPopupTrigger(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                Object source = e.getSource();
                if (source instanceof Tree) {
                    Tree tree = (Tree) source;
                    if (SwingUtilities.isRightMouseButton(e)) {
                        int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
                        if (pointedRow != -1) {
                            if (!tree.isRowSelected(pointedRow)) {
                                tree.setSelectionRow(pointedRow);
                            }
                        }
                    }
                    onPopupTrigger(e);
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                updateDeviceSelection((Tree) e.getSource());
                if (e.getClickCount() == 2) {
                    popupMenu.lauchMonitorDevice();
                }
            }
        };

        // TangoDeviceTree
        final ISourceDevice deviceSource = controller.getDeviceSource();
        SwingDeviceSelector deviceSelector = new SwingDeviceSelector(deviceSource, true, null, null, null, true);
        JComponent selectorComponent = (JComponent) deviceSelector.getSelectorComponent();
        add(selectorComponent, "Device");
        Component[] components = selectorComponent.getComponents();
        for (Component comp : components) {
            if (comp instanceof JScrollPane) {
                devicePane = (JScrollPane) comp;
                break;
            }
        }
        deviceTree = (Tree) deviceSelector.getTree();
        CSMonitorTreeCellRenderer renderer = new CSMonitorTreeCellRenderer(controller, true);
        deviceTree.setCellRenderer(renderer);
        deviceTree.addMouseListener(mouseListener);

        deviceTree.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                TreeNode searchNode = null;
                TreeNode goodNode = null;
                char keyChar = e.getKeyChar();

                String valueOf = String.valueOf(keyChar);
                TreePath path;
                TreePath current = deviceTree.getSelectionPath();

                // If it's the first put in the keyboard
                if (firstPress) {
                    oldKeyPress = valueOf;
                    goodNode = redirectionTree(current, searchNode, keyChar);
                    firstPress = false;
                } else {
                    if (!oldKeyPress.equals(valueOf)) {
                        oldKeyPress = valueOf;
                        countPressKey = 0;
                        goodNode = redirectionTree(current, searchNode, keyChar);
                    } else if (oldKeyPress.equals(valueOf)) {
                        countPressKey = countPressKey + 1;
                        TreeNode redirectionTree = redirectionTree(current, searchNode, keyChar);
                        if ((redirectionTree != null) && (redirectionTree.getParent() != null)) {
                            int index = redirectionTree.getParent().getIndex(redirectionTree);
                            TreeNode nextNode = getNextNode(index + countPressKey);
                            goodNode = nextNode;
                        }
                    }
                }
                if (goodNode != null) {
                    path = createTreePath(goodNode);
                    deviceTree.setSelectionPath(path);
                    deviceTree.scrollPathToVisible(path);
                }
            }
        });

        // TangoServerTree
        ISourceDevice serverSource = controller.getServerSource();
        SwingDeviceSelector serverSelector = new SwingDeviceSelector(serverSource, true, null, null, null, true);
        selectorComponent = (JComponent) serverSelector.getSelectorComponent();
        add(selectorComponent, "Server");
        components = selectorComponent.getComponents();
        for (Component comp : components) {
            if (comp instanceof JScrollPane) {
                serverPane = (JScrollPane) comp;
                break;
            }
        }
        serverTree = (Tree) serverSelector.getTree();
        CSMonitorTreeCellRenderer renderer2 = new CSMonitorTreeCellRenderer(controller, false);
        serverTree.setCellRenderer(renderer2);
        serverTree.addMouseListener(mouseListener);

        refreshpopupMenu = new JPopupMenu();
        menuRefreshAll = new JMenuItem("Refresh all");
        ImageIcon refreshIcon = ResourcesUtil.getIcon("CSMonitor.RefreshIcon");
        menuRefreshAll.setIcon(refreshIcon);

        refreshpopupMenu.add(menuRefreshAll);

        menuRefreshAll.addActionListener((e) -> {
            controller.refreshState();
        });

        controller.setLoadingMessage("Tree created");
    }

    private TreePath createTreePath(TreeNode node) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (node != null) {
            stack.push(node);
            node = node.getParent();
        }
        return new TreePath(stack.toArray());
    }

    private TreeNode redirectionTree(TreePath current, TreeNode searchNode, char keyChar) {
        TreeNode goodNode = null;
        if (current == null) {
            searchNode = deviceTree.getModel().getRoot();
        } else {
            searchNode = getNextSibling((TreeNode) current.getLastPathComponent());
        }

        if (searchNode != null) {
            DefaultMutableTreeNode root = deviceTree.getModel().getRoot();
            int childCount = deviceTree.getModel().getChildCount(root);

            for (int i = 0; i < childCount; i++) {
                TreeNode childAt = root.getChildAt(i);
                String childAtString = childAt.toString();

                if (childAtString.contains("name")) {
                    String[] split = childAtString.split("name: ");
                    String childName = split[1].trim();
                    if (childName.startsWith(String.valueOf(keyChar))) {
                        goodNode = childAt;
                        break;
                    }
                }
            }
        }
        return goodNode;
    }

    private TreeNode getNextSibling(TreeNode node) {
        TreeNode parent = node.getParent();
        while (parent != null) {
            int index = parent.getIndex(node) + 1;
            if (index < parent.getChildCount()) {
                return parent.getChildAt(index);
            }
            node = parent;
            parent = node.getParent();
        }
        return null;
    }

    private TreeNode getNextNode(int indexOfOldNode) {
        DefaultMutableTreeNode root = deviceTree.getModel().getRoot();
        TreeNode nextNode = root.getChildAt(indexOfOldNode);
        return nextNode;
    }

    private boolean isDeviceSourceSelected() {
        return this.getSelectedIndex() == 0;
    }

    private void updateDeviceSelection(Tree tree) {
        SELECTED_DEVICE_NAME.clear();
        if (tree != null) {
            ITreeNode[] selectedNodes = tree.getSelectedNodes();
            if ((selectedNodes != null) && (selectedNodes.length > 0)) {
                String deviceName = null;
                for (ITreeNode selectedNode : selectedNodes) {
                    deviceName = controller.getDeviceName(String.valueOf(selectedNode.getData()));
                    if (deviceName != null && !deviceName.isEmpty()) {
                        SELECTED_DEVICE_NAME.add(deviceName.toLowerCase());
                    }
                }

                popupMenu.setSelectedDevice(SELECTED_DEVICE_NAME);
                CSEvent event = new CSEvent(this, ResourcesUtil.getFirstElement(SELECTED_DEVICE_NAME),
                        EventType.SELECTED_DEVICE);
                controller.fireSystemControlChanged(event);
            }
        }
    }

    private void onPopupTrigger(final MouseEvent e) {
        if (e.isPopupTrigger()) {
            Tree tree = (Tree) e.getSource();
            int pointedRow = tree.getRowForLocation(e.getX(), e.getY());
            if (pointedRow != -1) {
                ITreeNode[] selectedNodes = tree.getSelectedNodes();
                if ((selectedNodes != null) && (selectedNodes.length > 0)) {
                    if (selectedNodes[0].getParent() == null) {
                        refreshpopupMenu.show(e.getComponent(), e.getX(), e.getY());
                    }
                    for (ITreeNode selectedNode : selectedNodes) {
                        List<ITreeNode> children = selectedNode.getChildren();
                        if (selectedNode.getChildren() == null || children.isEmpty()) {
                            popupMenu.show(e.getComponent(), e.getX(), e.getY());
                            boolean deviceTree = isDeviceSourceSelected();
                            popupMenu.popupMenuEnable(!deviceTree, deviceTree);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void systemControlChanged(final CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.STATE_REFRESHED, eventType)) {
            Tree tree = isDeviceSourceSelected() ? deviceTree : serverTree;
            if (tree != null) {
                tree.repaint();
            }
        }
        if (ObjectUtils.sameObject(EventType.SET_DEVICE_SELECTED, eventType)) {
            boolean deviceSource = (Boolean) event.getSource();
            Object information = event.getInformation();
            if (information instanceof String) {
                String deviceName = (String) information;
                Tree tree = deviceTree;
                ITreeNode node = null;
                if (deviceSource) {
                    String[] split = deviceName.split(ISourceDevice.DEFAULT_SEPARATOR);
                    ITreeNode rootNode = tree.getRootNode();
                    List<ITreeNode> domain = rootNode.getChildren();
                    List<ITreeNode> family = null;
                    List<ITreeNode> member = null;

                    if (split != null && split.length <= 3) {
                        treepathfound: for (ITreeNode domainNode : domain) {
                            if (domainNode.getName().equalsIgnoreCase(split[0])) {
                                family = domainNode.getChildren();
                                for (ITreeNode familyNode : family) {
                                    if (familyNode.getName().equalsIgnoreCase(split[1])) {
                                        member = familyNode.getChildren();
                                        for (ITreeNode memberNode : member) {
                                            if (memberNode.getName().equalsIgnoreCase(split[2])) {
                                                node = memberNode;
                                                break treepathfound;
                                            }
                                        }
                                    }
                                }
                            }
                        } // end treepathfound
                    } // end if (split != null && split.length <= 3)
                } else {
                    tree = serverTree;
                    DeviceModel deviceModel = controller.getDeviceModel(deviceName);
                    if (deviceModel != null) {
                        DeviceInfo deviceInfo = deviceModel.getDeviceInfo();
                        if (deviceInfo != null) {
                            String serverName = deviceInfo.server;
                            if (serverName != null) {
                                String[] split = serverName.split(ISourceDevice.DEFAULT_SEPARATOR);
                                if (split != null && split.length <= 2) {
                                    ITreeNode rootNode = tree.getRootNode();
                                    List<ITreeNode> servers = rootNode.getChildren();
                                    List<ITreeNode> instance = null;
                                    List<ITreeNode> devices = null;
                                    treepathfound: for (ITreeNode serverNode : servers) {
                                        if (serverNode.getName().equalsIgnoreCase(split[0])) {
                                            instance = serverNode.getChildren();
                                            for (ITreeNode instanceNode : instance) {
                                                if (instanceNode.getName().equalsIgnoreCase(split[1])) {
                                                    devices = instanceNode.getChildren();
                                                    for (ITreeNode deviceNode : devices) {
                                                        if (deviceNode.getName().equalsIgnoreCase(deviceName)) {
                                                            node = deviceNode;
                                                            break treepathfound;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } // end treepathfound
                                } // end if (split != null && split.length <= 2)
                            } // end if (serverName != null)
                        } // end if (deviceInfo != null)
                    } // end if (deviceModel != null)
                } // end if (deviceSource) ... else

                if (tree != null && node != null) {
                    JScrollPane scrollPane = devicePane;
                    if (deviceSource) {
                        setSelectedIndex(0);
                    } else {
                        scrollPane = serverPane;
                        setSelectedIndex(1);
                    }

                    tree.selectNodes(false, node);
                    int[] selectionRows = tree.getSelectionRows();
                    if (selectionRows != null && selectionRows.length > 0) {
                        Rectangle rowBounds = tree.getRowBounds(selectionRows[0]);
                        scrollPane.getViewport().scrollRectToVisible(rowBounds);
                    }

                    updateDeviceSelection(tree);

                }
            } // end if (information instanceof String)
        } // end if (ObjectUtils.sameObject(EventType.SET_DEVICE_SELECTED, eventType))
    }
}
