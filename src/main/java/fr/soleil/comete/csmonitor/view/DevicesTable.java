package fr.soleil.comete.csmonitor.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.JXTable;

import fr.esrf.Tango.DevState;
import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.lib.project.ObjectUtils;

public class DevicesTable extends JXTable implements ICSMonitorListener {

    private static final long serialVersionUID = -4005361173536008735L;

    public static enum TableType {
        SUBDEVICE, RELATED
    };

    private ICSMonitor controller;
    private List<String> deviceList;
    private DeviceTableModel tableModel;

    private TableType tableType = TableType.SUBDEVICE;
    private final DevicePopupMenu popupMenu;

    public DevicesTable(final ICSMonitor controller, final TableType tableType, final DevicePopupMenu deviceMenu) {
        this.controller = controller;
        this.tableType = tableType;
        this.popupMenu = deviceMenu;
        tableModel = new DeviceTableModel();
        setModel(tableModel);

        controller.addCSMonitorListener(this);
        setShowGrid(false);
        setShowHorizontalLines(true);
        setShowVerticalLines(true);
        setFillsViewportHeight(true);
        setEditable(false);

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = rowAtPoint(e.getPoint());
                int col = columnAtPoint(e.getPoint());
                if (col == 0) {
                    Object valueAt = getValueAt(row, col + 1);
                    String devicePath = String.valueOf(valueAt);
                    if ((devicePath != null) && (row >= 0 && col >= 0)) {
                        setRowSelectionInterval(row, row);
                        if (SwingUtilities.isRightMouseButton(e)) {
                            popupMenu.setSelectedDevice(devicePath);
                            popupMenu.show(e.getComponent(), e.getX(), e.getY());
                            popupMenu.popupMenuEnable(true, true);
                        }
                    }
                }
            }
        });
    }

    public class DeviceTableModel extends AbstractTableModel {

        private static final long serialVersionUID = 8538477531673754876L;

        String title = TableType.RELATED.equals(tableType) ? "Related device(s)" : "Sub device(s)";
        private String[] columnName = new String[] { "Link", title };

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object value = null;
            if (deviceList != null && rowIndex < deviceList.size()) {
                String deviceName = deviceList.get(rowIndex);
                if (columnIndex == 0) {
                    DeviceModel deviceModel = controller.getDeviceModel(deviceName);
                    if (deviceModel != null) {
                        DevState state = deviceModel.getState();
                        value = ResourcesUtil.getIconForState(state);
                    }
                } else {
                    value = deviceName;
                }
            }

            return value;
        }

        @Override
        public int getRowCount() {
            int rowCount = 0;
            if (deviceList != null) {
                rowCount = deviceList.size();
            }
            return rowCount;
        }

        @Override
        public int getColumnCount() {
            return columnName.length;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            Class<?> classType = String.class;
            if (columnIndex == 0) {
                classType = ImageIcon.class;
            }
            return classType;
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnName[columnIndex];
        }
    };

    @Override
    public void systemControlChanged(final CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.SELECTED_DEVICE, eventType)) {
            String deviceName = String.valueOf(event.getInformation());
            DeviceModel deviceModel = controller.getDeviceModel(deviceName);
            if (deviceModel != null) {
                deviceList = TableType.RELATED.equals(tableType) ? deviceModel.getRelatedDevice()
                        : deviceModel.getSubDevice();
                tableModel.fireTableStructureChanged();
                setColumnWidth(0, 75);
            }
        }
        if (ObjectUtils.sameObject(EventType.STATE_REFRESHED, eventType)) {
            tableModel.fireTableDataChanged();
            setColumnWidth(0, 75);
        }
    }

    private void setColumnWidth(int col, int with) {
        TableColumnModel columnModel = getColumnModel();
        if (columnModel != null) {
            TableColumn column = columnModel.getColumn(col);
            column.setMaxWidth(with);
            column.setMinWidth(with);
            column.setResizable(false);
        }
    }

}
