package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.ObjectUtils;

public class DeviceStatePanel extends AbstractTangoBox implements ICSMonitorListener {

    private static final long serialVersionUID = 6788028177688191744L;

    private JPanel panel;
    private TextArea statusArea;
    private JLabel deviceNameLabel;

    public DeviceStatePanel(final ICSMonitor controller) {
        controller.addCSMonitorListener(this);
        initComponent();
    }

    private void initComponent() {
        setLayout(new BorderLayout());
        panel = new JPanel(new BorderLayout());
        deviceNameLabel = new JLabel();
        statusArea = new TextArea();
        panel.add(getStateLabel(), BorderLayout.NORTH);
        panel.add(statusArea, BorderLayout.CENTER);
        add(deviceNameLabel, BorderLayout.NORTH);
        add(panel, BorderLayout.CENTER);
    }

    @Override
    public void systemControlChanged(final CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.SELECTED_DEVICE, eventType)) {
            String deviceName = String.valueOf(event.getInformation());
            clearGUI();
            setModel(deviceName);
            start();
            refreshGUI();
        }
    }

    @Override
    protected void clearGUI() {
        getStateLabel().setOpaque(false);
        cleanStateModel();
//        stringBox.disconnectWidgetFromAll(getStateLabel());
//        getStateLabel().setBackground(UNKNOWColor);
//        getStateLabel().setCometeBackground(CometeColor.GRAY);
        cleanWidget(statusArea);
        stringBox.disconnectWidgetFromAll(statusArea);
        deviceNameLabel.setText("");
    }

    @Override
    protected void refreshGUI() {
        // System.out.println("getModel()=" + getModel());
        // System.out.println("refreshGUI");
        if (getModel() != null && !getModel().trim().isEmpty()) {
            panel.setVisible(true);
            getStateLabel().setOpaque(true);
            setStateModel();
            deviceNameLabel.setText(getModel());
            IKey statusKey = generateAttributeKey("Status");
            stringBox.setColorEnabled(statusArea, false);
            setWidgetModel(statusArea, stringBox, statusKey);
        } else {
            panel.setVisible(false);
        }
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

}
