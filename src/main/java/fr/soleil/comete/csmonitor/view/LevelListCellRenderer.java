package fr.soleil.comete.csmonitor.view;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.soleil.comete.swing.util.Level;

@Deprecated
public class LevelListCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -7590225684758904027L;

    private final Map<Level, Icon> icons;

    public LevelListCellRenderer() {
        super();
        icons = new HashMap<>();
    }

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        Icon icon = null;
        if (value instanceof Level) {
            synchronized (icons) {
                icon = icons.get(value);
            }
            if (Level.WARN.equals(value)) {
                label.setText("warning");
            } else {
                label.setText(label.getText().toLowerCase());
            }
        }
        label.setIcon(icon);
        return label;
    }

    public void addIcon(Level level, Icon icon) {
        if ((level != null) && (icon != null)) {
            synchronized (icons) {
                icons.put(level, icon);
            }
        }
    }

    public void removeIcon(Level level) {
        if (level != null) {
            synchronized (icons) {
                icons.remove(level);
            }
        }
    }

    public void clearIcons() {
        synchronized (icons) {
            icons.clear();
        }
    }

}
