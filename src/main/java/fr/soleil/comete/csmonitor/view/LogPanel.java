package fr.soleil.comete.csmonitor.view;

import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;

import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

public class LogPanel extends JTabbedPane implements ICSMonitorListener {

    private static final long serialVersionUID = -4666839415289322951L;

    private TextArea logTextArea;
    private LogViewerTable history;
    private ICSMonitor controller;

    public LogPanel(final ICSMonitor controller) {
        this.controller = controller;
        this.controller.addCSMonitorListener(this);
        initComponent();
    }

    private void initComponent() {
        logTextArea = new TextArea();
        logTextArea.setRows(6);

        history = new LogViewerTable();
        history.setModel(controller.getControlMonitorDevice());
        history.start();

        ImageIcon logIcon = ResourcesUtil.getIcon("CSMonitor.Log");
        ImageIcon historyIcon = ResourcesUtil.getIcon("CSMonitor.History");

        addTab("Logs", logIcon, logTextArea);
        addTab("Historic", historyIcon, history);
    }

    @Override
    public void systemControlChanged(final CSEvent event) {
        if (logTextArea != null) {
            EventType eventType = event.getEventType();
            if (ObjectUtils.sameObject(EventType.LOG_CHANGED, eventType)) {
                String message = String.valueOf(event.getInformation());
                String text = logTextArea.getText();
                logTextArea
                        .setText(text + ObjectUtils.NEW_LINE + event.getSource() + " " + message + ObjectUtils.NEW_LINE
                                + DateUtil.milliToString(System.currentTimeMillis(), ResourcesUtil.DATE_FORMAT));
            }
        }
    }

}
