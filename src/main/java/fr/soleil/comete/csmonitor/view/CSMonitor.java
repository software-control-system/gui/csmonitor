package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.csmonitor.controller.CSMonitorController;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.SimulatedControlMonitor;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.SystemUtils;
import fr.soleil.lib.project.file.BatchExecutor;

public class CSMonitor extends JFrame {

    private static final long serialVersionUID = -5996926454636462472L;

    private static final String FRAME_TITLE = CSMonitor.class.getSimpleName();
    private static final boolean SIMULATED = SystemUtils.getSystemBooleanProperty("SIMULED", false);

    private JFrame parentFrame;

    private JSplitPane splitPaneTreeDevicePanel;
    private JSplitPane splitPane;
    private JMenuBar menuBar;
    private JMenu menuFile;
    private JMenu menuPreference;
    private JMenu menuHelp;
    private JMenu menuLaunch;
    private JPanel mainPanel;

    private JMenuItem menuFileExit;
    private JMenuItem menuHelpAbout;
    private JMenuItem menuControlPanelPreference;
    private JMenuItem menuAstorPanelPreference;
    private JMenuItem menuJivePanelPreference;
    private JMenuItem menuRefreshAll;
    private JCheckBoxMenuItem menuAskConfirmationPreference;
    private JCheckBoxMenuItem menuShowSubdevicesPanel;

    private JMenuItem menuLaunchAstor;
    private JMenuItem menuLaunchJive;

    private static ICSMonitor controller;
    private DeviceTreePanel deviceTreePanel;
    private DevicePanel devicePanel;

    private LogPanel logPanel;

    private PathPanelDialog controlPanelDialog;
    private BatchExecutor astorBatch;
    private BatchExecutor jiveBatch;

    // Constructor without arg
    public CSMonitor() {
        initComponent(false, null);
    }

    // Constructor with arg
    public CSMonitor(String deviceName) {
        initComponent(true, deviceName);
    }

    public static ICSMonitor getController() {
        return controller;
    }

    private void initComponent(boolean containsArgs, String deviceName) {

        if (SIMULATED) {
            controller = new SimulatedControlMonitor();
        } else if (containsArgs && (deviceName != null)) {
            controller = new CSMonitorController(deviceName);
        } else {
            controller = new CSMonitorController();
        }

        // Set parent of waiting dialog
        setBounds(PreferenceFile.getWindowBounds());
        controller.showWaitingDialog(this, ObjectUtils.EMPTY_STRING);
        DevicePopupMenu popupMenu = new DevicePopupMenu(controller);
        deviceTreePanel = new DeviceTreePanel(controller, popupMenu);
        devicePanel = new DevicePanel(controller, popupMenu);

        controller.setLoadingMessage("Create log panel");
        logPanel = new LogPanel(controller);
        controller.increaseLoadingProgress(5);

        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());

        controller.setLoadingMessage("Create menu file");
        menuBar = new JMenuBar();
        menuFile = new JMenu("File");
        menuPreference = new JMenu("Preference");
        menuHelp = new JMenu("Help");

        splitPaneTreeDevicePanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, deviceTreePanel, devicePanel);
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, splitPaneTreeDevicePanel, logPanel);

        menuFileExit = new JMenuItem("Exit");
        ImageIcon exitIcon = ResourcesUtil.getIcon("CSMonitor.Exit");
        menuFileExit.setIcon(exitIcon);

        menuHelpAbout = new JMenuItem("About...");
        ImageIcon aboutIcon = ResourcesUtil.getIcon("CSMonitor.AboutIcon");
        menuHelpAbout.setIcon(aboutIcon);

        menuLaunch = new JMenu("Launch");

        menuLaunchAstor = new JMenuItem("Astor");
        ImageIcon astorIcon = ResourcesUtil.getIcon("CSMonitor.AstorIcon");
        menuLaunchAstor.setIcon(astorIcon);

        menuLaunchJive = new JMenuItem("Jive");
        ImageIcon jiveIcon = ResourcesUtil.getIcon("CSMonitor.JiveIcon");
        menuLaunchJive.setIcon(jiveIcon);

        menuLaunchAstor
                .setAccelerator(KeyStroke.getKeyStroke('A', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuLaunchJive
                .setAccelerator(KeyStroke.getKeyStroke('J', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));

        menuRefreshAll = new JMenuItem("Refresh all");
        menuRefreshAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
        menuRefreshAll.setIcon(ResourcesUtil.getIcon("CSMonitor.RefreshIcon"));

        menuControlPanelPreference = new JMenuItem("Monitor path");
        menuControlPanelPreference.setIcon(ResourcesUtil.getDecorableIcon("CSMonitor.MonitorIcon", "CSMonitor.Edit"));
        menuAstorPanelPreference = new JMenuItem("Astor path");
        menuAstorPanelPreference.setIcon(ResourcesUtil.getDecorableIcon("CSMonitor.AstorIcon", "CSMonitor.Edit"));
        menuJivePanelPreference = new JMenuItem("Jive path");
        menuJivePanelPreference.setIcon(ResourcesUtil.getDecorableIcon("CSMonitor.JiveIcon", "CSMonitor.Edit"));
        menuAskConfirmationPreference = new JCheckBoxMenuItem("Ask confirmation before init execution");
        menuAskConfirmationPreference
                .setIcon(ResourcesUtil.getDecorableIcon("CSMonitor.AskConfirmationIcon", "CSMonitor.Edit"));

        menuShowSubdevicesPanel = new JCheckBoxMenuItem("Replace the Tree subdevices by a panel");
        menuShowSubdevicesPanel
                .setIcon(ResourcesUtil.getDecorableIcon("CSMonitor.ShowSubdevicePanel", "CSMonitor.Edit"));

        menuAskConfirmationPreference.setSelected(PreferenceFile.getAskDialogConfirmationValue());

        System.out.println("PreferenceFile.getShowSubdevicesPanel() = " + PreferenceFile.getShowSubdevicesPanel());
        menuShowSubdevicesPanel.setSelected(PreferenceFile.getShowSubdevicesPanel());

        menuPreference.add(menuControlPanelPreference);
        menuPreference.add(menuAstorPanelPreference);
        menuPreference.add(menuJivePanelPreference);
        menuPreference.addSeparator();
        menuPreference.add(menuAskConfirmationPreference);
        menuPreference.addSeparator();
        menuPreference.add(menuShowSubdevicesPanel);

        menuLaunch.add(menuLaunchAstor);
        menuLaunch.add(menuLaunchJive);

        menuFile.add(menuLaunch);
        menuFile.addSeparator();
        menuFile.add(menuRefreshAll);
        menuFile.addSeparator();
        menuFile.add(menuFileExit);

        menuHelp.add(menuHelpAbout);

        menuBar.add(menuFile);
        menuBar.add(menuPreference);

        menuBar.add(menuHelp);

        menuFileExit.addActionListener((e) -> {
            exitCSMonitor();
            System.exit(0);
        });

        menuRefreshAll.addActionListener((e) -> {
            controller.refreshState();
        });

        menuHelpAbout.addActionListener((e) -> {
            AboutDialog aboutDialog = new AboutDialog(parentFrame);
            aboutDialog.setVisible(true);
            aboutDialog.toFront();
        });

        menuControlPanelPreference.addActionListener((e) -> {
            controlPanelDialog = getPathPanelDialog();
            controlPanelDialog.setIconImage(((ImageIcon) menuControlPanelPreference.getIcon()).getImage());
            controlPanelDialog.setTitle(menuControlPanelPreference.getText());
            controlPanelDialog.setBatchType(PathPanelDialog.BatchType.CONTROL);
            controlPanelDialog.setVisible(true);
        });

        menuAstorPanelPreference.addActionListener((e) -> {
            controlPanelDialog = getPathPanelDialog();
            controlPanelDialog.setIconImage(((ImageIcon) menuAstorPanelPreference.getIcon()).getImage());
            controlPanelDialog.setTitle(menuAstorPanelPreference.getText());
            controlPanelDialog.setBatchType(PathPanelDialog.BatchType.ASTOR);
            controlPanelDialog.setVisible(true);
        });

        menuJivePanelPreference.addActionListener((e) -> {
            controlPanelDialog = getPathPanelDialog();
            controlPanelDialog.setIconImage(((ImageIcon) menuJivePanelPreference.getIcon()).getImage());
            controlPanelDialog.setTitle(menuJivePanelPreference.getText());
            controlPanelDialog.setBatchType(PathPanelDialog.BatchType.JIVE);
            controlPanelDialog.setVisible(true);
        });

        menuLaunchAstor.addActionListener((e) -> {
            if (astorBatch == null) {
                astorBatch = new BatchExecutor();
            }
            String astorCmd = PreferenceFile.getAstorPath();
            if (astorCmd != null && !astorCmd.isEmpty()) {
                controller.logMessage(this.getClass().getSimpleName(), "Execute command " + astorCmd);
                astorBatch.setBatch(astorCmd);
                astorBatch.execute();
            }
        });

        menuLaunchJive.addActionListener((e) -> {
            if (jiveBatch == null) {
                jiveBatch = new BatchExecutor();
            }
            String jiveCmd = PreferenceFile.getJivePath();
            if (jiveCmd != null && !jiveCmd.isEmpty()) {
                controller.logMessage(this.getClass().getSimpleName(), "Execute command " + jiveCmd);
                jiveBatch.setBatch(jiveCmd);
                jiveBatch.execute();
            }
        });

        menuAskConfirmationPreference.addItemListener((e) -> {
            if (e.getStateChange() > 0) {
                PreferenceFile.setAskDialogConfirmation(menuAskConfirmationPreference.getState());
            }
        });

        menuShowSubdevicesPanel.addItemListener((e) -> {
            if (e.getStateChange() > 0) {
                devicePanel.changeSubdevicesPanel(menuShowSubdevicesPanel.getState());
                PreferenceFile.setShowSubdevicesPanel(menuShowSubdevicesPanel.getState());
            }
        });

        splitPaneTreeDevicePanel.setOneTouchExpandable(true);
        splitPane.setOneTouchExpandable(true);

        splitPaneTreeDevicePanel.setDividerLocation(PreferenceFile.getTreeDividerLocation());
        splitPane.setDividerLocation(PreferenceFile.getLogDividerLocation());

        mainPanel.add(splitPane, BorderLayout.CENTER);

        String version = ResourcesUtil.getFileJarVersion(CSMonitor.class);
        String title;
        if ((version != null) && (!version.isEmpty())) {
            title = FRAME_TITLE + " - " + version;
        } else {
            title = FRAME_TITLE;
        }
        ImageIcon frameIcon = ResourcesUtil.getIcon("CSMonitor.Frame");
        setIconImage(frameIcon.getImage());
        setJMenuBar(menuBar);
        add(mainPanel);
        setTitle(title);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//      setSize(1000, 1000);
        pack();
        Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        Dimension size = getSize();
        int width, height;
        width = Math.min(Math.max(size.width, 1000), bounds.width);
        height = Math.min(Math.max(size.height, 1000), bounds.height);
        setSize(width, height);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                exitCSMonitor();
            }
        });
        controller.increaseLoadingProgress(5);

        controller.setLoadingMessage("Application ready");
        controller.setLoadingMessageVisible(false);
    }

    private void exitCSMonitor() {
        Rectangle windowBounds = getBounds();
        PreferenceFile.setWindowBounds(windowBounds);
        PreferenceFile.setTreeDividerLocation(splitPaneTreeDevicePanel.getDividerLocation());
        PreferenceFile.setLogDividerLocation(splitPane.getDividerLocation());

        devicePanel.saveSplitPaneLocation();
        PreferenceFile.saveSettings();
    }

    private PathPanelDialog getPathPanelDialog() {
        if (controlPanelDialog == null) {
            controlPanelDialog = new PathPanelDialog(this);
        }
        return controlPanelDialog;
    }

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    CSMonitor monitor = new CSMonitor();
                    monitor.setLocationRelativeTo(null);
                    monitor.setVisible(true);
                    monitor.toFront();
                }
            });
        } else if (args != null || args.length == 1) {
            String deviceName = args[0];
            CSMonitor monitor = new CSMonitor(deviceName);
            monitor.setLocationRelativeTo(null);
            monitor.setVisible(true);
            monitor.toFront();
        }
    }
}
