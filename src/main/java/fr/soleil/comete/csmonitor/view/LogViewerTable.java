package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.target.DeviceLogSpectrumAdapter;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.lib.project.application.logging.LogViewer;

/**
 * This bean manages the logs part of the passerelle device
 * 
 * @author saintin
 * 
 */
public class LogViewerTable extends AbstractTangoBox {

    private static final long serialVersionUID = -3736622897072677364L;

    // the text area that displays history
    private LogViewer historyViewer;
    private DeviceLogSpectrumAdapter logSpectrumAdapter;
    private JScrollPane historyScrollPane;
//    // the combobox to filter what kind of history you wan to view
//    private JComboBox<Level> levelFilterComboBox;
    private boolean displayMessageOnConnectionError;

    private final StringMatrixBox stringMatrixBox;

    public LogViewerTable() {
        super();
        displayMessageOnConnectionError = true;
        stringMatrixBox = (StringMatrixBox) CometeBoxProvider.getCometeBox(StringMatrixBox.class);
        initComponents();
        layoutComponents();
    }

    // components initialization
    private void initComponents() {
        historyViewer = new LogViewer(getClass().getName());
        logSpectrumAdapter = new DeviceLogSpectrumAdapter(historyViewer);
        historyScrollPane = new JScrollPane(historyViewer);

    }

    // layout components in panel
    private void layoutComponents() {
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        setLayout(new BorderLayout(0, 5));
        add(historyScrollPane, BorderLayout.CENTER);
    }

    @Override
    protected void clearGUI() {
        cleanWidget(logSpectrumAdapter);
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void onConnectionError() {
        if (isDisplayMessageOnConnectionError()) {
            showMessageDialog(this, "Failed to connect to DataRecorder", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void refreshGUI() {
        setWidgetModel(logSpectrumAdapter, stringMatrixBox, generateAttributeKey("historic"));
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public boolean isDisplayMessageOnConnectionError() {
        return displayMessageOnConnectionError;
    }

    public void setDisplayMessageOnConnectionError(boolean displayMessageOnConnectionError) {
        this.displayMessageOnConnectionError = displayMessageOnConnectionError;
    }

}
