package fr.soleil.comete.csmonitor.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;

import fr.esrf.Tango.DevState;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitor.NodeState;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.comete.csmonitor.util.TangoColorUtil;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;

public class CSMonitorTreeCellRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = 6388287646234396257L;

    private ICSMonitor controller;
    private boolean deviceTree;

    public CSMonitorTreeCellRenderer(ICSMonitor controller, boolean deviceTree) {
        this.controller = controller;
        this.deviceTree = deviceTree;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        Component superComponent = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        ITreeNode treeNode = getTreeNode(value, tree);
        if (treeNode != null) {

            String deviceName = getDeviceName(treeNode);
            Color colorForState = Color.BLACK;
            ImageIcon icon = null;

            DeviceModel deviceModel = controller.getDeviceModel(deviceName);
            if (deviceModel != null) {
                DevState deviceState = deviceModel.getState();
                if (deviceState == DevState.UNKNOWN) {
                    colorForState = TangoColorUtil.getColorForState(deviceState.toString());
                }
                icon = ResourcesUtil.getIconForState(deviceState);
            } else {
                NodeState nodeState = controller.getNodeState((String) treeNode.getData(), deviceTree);
                icon = ResourcesUtil.getIconForNodeState(nodeState);
            }
            if (superComponent != null && superComponent instanceof JLabel) {
                superComponent.setForeground(colorForState);
                if (icon != null) {
                    JLabel label = (JLabel) superComponent;
                    label.setIcon(icon);
                }
            }
        }
        return superComponent;
    }

    private String getDeviceName(ITreeNode treeNode) {
        String deviceName = null;
        if (treeNode != null) {
            deviceName = controller.getDeviceName(String.valueOf(treeNode.getData()));
            // System.out.println("deviceName=" + deviceName);
            // return only the leaf selection
            if (treeNode != null) {
                ITreeNode parent = treeNode.getParent();
                if (parent != null) {
                }
            }
        }
        return deviceName;

    }
}
