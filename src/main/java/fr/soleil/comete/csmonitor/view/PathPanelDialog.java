package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EventObject;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.soleil.lib.project.ObjectUtils;

public class PathPanelDialog extends JDialog {

    private static final long serialVersionUID = -385995986119765391L;

    protected JPanel mainPanel;
    protected JPanel panelBtn;

    private FileBrowserField browser;
    protected JButton btnOk;
    protected JButton btnCancel;
    private Component parentRelative;

    public static enum BatchType {
        NONE, CONTROL, JIVE, ASTOR
    };

    private BatchType batchType = BatchType.NONE;

    public PathPanelDialog(final Component component) {
        super(SwingUtilities.getWindowAncestor(component));
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cancel();
            }
        });
        parentRelative = component;
        setModal(true);
        setAlwaysOnTop(true);
        mainPanel = new JPanel(new BorderLayout());

        browser = new FileBrowserField() {
            private static final long serialVersionUID = 3121787964248465771L;

            @Override
            public void actionPerformed(EventObject event) {
                ok();
            }
        };
        browser.setSendButtonVisible(false);

        panelBtn = new JPanel();
        btnOk = new JButton("OK");
        btnCancel = new JButton("Cancel");

        btnOk.addActionListener((e) -> {
            ok();
        });

        btnCancel.addActionListener((e) -> {
            cancel();
        });

        setSize(300, 100);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(component);

        mainPanel.add(panelBtn, BorderLayout.SOUTH);
        mainPanel.add(browser, BorderLayout.CENTER);
        panelBtn.add(btnOk);
        panelBtn.add(btnCancel);

        setContentPane(mainPanel);
    }

    protected void ok() {
        String path = browser.getText();
        if (path != null && !path.isEmpty()) {
            switch (batchType) {
                case CONTROL:
                    PreferenceFile.setControlPanelPath(path);
                    break;

                case JIVE:
                    PreferenceFile.setJivePath(path);
                    break;

                case ASTOR:
                    PreferenceFile.setAstorPath(path);
                    break;

                default:
                    break;

            }
            PreferenceFile.saveSettings();
        }
        setVisible(false);
    }

    protected void cancel() {
        setBatchType(batchType);
        setVisible(false);
    }

    @Override
    public void setVisible(boolean visible) {
        setLocation(parentRelative.getLocationOnScreen());
        super.setVisible(visible);
    }

    public void setBatchType(final BatchType batchType) {
        this.batchType = batchType;
        switch (batchType) {
            case CONTROL:
                browser.setText(PreferenceFile.getControlPanelPath());
                break;

            case ASTOR:
                browser.setText(PreferenceFile.getAstorPath());
                break;

            case JIVE:
                browser.setText(PreferenceFile.getJivePath());
                break;

            default:
                browser.setText(ObjectUtils.EMPTY_STRING);
                break;

        }

    }

}
