package fr.soleil.comete.csmonitor.view;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.lib.project.ObjectUtils;

public class SubdevicesTree extends JPanel implements ICSMonitorListener {

    private static final long serialVersionUID = -6361195163942020708L;

    private ICSMonitor controller = null;

    private JTree subdevicesTree = null;
    private List<String> mySubDevicesList = null;
    private DevicePopupMenu popupMenu = null;

    public SubdevicesTree(ICSMonitor controller, DevicePopupMenu popupMenu) {
        this.controller = controller;
        this.popupMenu = popupMenu;
        controller.addCSMonitorListener(this);
    }

    @Override
    public void systemControlChanged(final CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.SELECTED_DEVICE, eventType)) {
            String deviceName = String.valueOf(event.getInformation());

            if (mySubDevicesList == null) {
                mySubDevicesList = new ArrayList<String>();
            }

            if (subdevicesTree == null) {
                createSubdevicesPanel(deviceName);
            } else {
                refreshSubdevicesPanel(deviceName);
            }
        }
    }

    private void refreshSubdevicesPanel(String deviceName) {
        DefaultTreeModel model = (DefaultTreeModel) subdevicesTree.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();

        root.removeAllChildren();
        model.reload(root);

        searchSubDevices(deviceName, root);
        model.reload(root);
    }

    private void searchSubDevices(String deviceName, DefaultMutableTreeNode root) {
        if (root != null && !deviceName.isEmpty()) {
            DeviceModel deviceModel = controller.getDeviceModel(deviceName);
            if (deviceModel != null) {
                mySubDevicesList = deviceModel.getSubDevice();
                if ((!mySubDevicesList.isEmpty()) && (mySubDevicesList != null) && (mySubDevicesList.size() > 0))
                    // SubDevices stage 1
                    for (String sub : mySubDevicesList) {
                        DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode(sub);
                        root.add(defaultMutableTreeNode);

                        DeviceModel deviceModel2 = controller.getDeviceModel(sub);
                        if (deviceModel2 != null) {
                            List<String> subDeviceList2 = deviceModel2.getSubDevice();
                            if ((!subDeviceList2.isEmpty()) && (subDeviceList2 != null) && (subDeviceList2.size() > 0))
                                // SubDevices stage 2
                                for (String sub2 : subDeviceList2) {
                                    MutableTreeNode childNode = new DefaultMutableTreeNode(sub2);
                                    defaultMutableTreeNode.add(childNode);
                                }
                        }
                    }
            }
        }
    }

    private void createSubdevicesPanel(final String deviceName) {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Subdevices");
        searchSubDevices(deviceName, root);
        subdevicesTree = new JTree(root);

        subdevicesTree.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (SwingUtilities.isRightMouseButton(e)) {

                    String lastPath = subdevicesTree.getSelectionPath().getLastPathComponent().toString();

                    popupMenu.setSelectedDevice(lastPath);
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    popupMenu.popupMenuEnable(true, true);
                }
            }
        });

        SubdevicesTreeRenderer subdevicesTreeRenderer = new SubdevicesTreeRenderer(controller);
        subdevicesTree.setCellRenderer(subdevicesTreeRenderer);

        SubdevicesTree.this.setLayout(new BorderLayout());
        SubdevicesTree.this.add(subdevicesTree, BorderLayout.CENTER);
    }
}
