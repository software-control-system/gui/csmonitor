package fr.soleil.comete.csmonitor.view;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import org.jdesktop.swingx.JXTable;

import fr.esrf.TangoApi.DeviceInfo;
import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.lib.project.ObjectUtils;

public class DeviceInformationTable extends JXTable implements ICSMonitorListener {

    private static final long serialVersionUID = 2708224609963615056L;

    private ICSMonitor controller;

    public DeviceInformationTable(final ICSMonitor controller) {
        this.controller = controller;
        controller.addCSMonitorListener(this);

        setShowGrid(false);
        setShowHorizontalLines(true);
        setShowVerticalLines(true);
        setFillsViewportHeight(true);
        updateTableInformation(null);
        setEditable(false);
    }

    public void updateTableInformation(DeviceModel deviceModel) {
        DeviceInfo deviceInfo = deviceModel != null ? deviceModel.getDeviceInfo() : null;
        if (deviceInfo != null) {
            String starterDev = deviceModel != null && deviceModel.isStartable()
                    ? " | Starter=" + deviceModel.getStarterDeviceName()
                    : ObjectUtils.EMPTY_STRING;
            String deviceNameInfo = deviceInfo.name;
            String hostTableName = deviceInfo.hostname + starterDev;
            String version = deviceInfo.version;
            String serverTableName = deviceInfo.server;
            int serveurPID = deviceInfo.pid;
            boolean exportedTable = deviceInfo.exported;
            String exported = String.valueOf(exportedTable);
            String lastExported = deviceInfo.last_exported;
            String lastUnexported = deviceInfo.last_unexported;
            DefaultTableModel tableModel = (DefaultTableModel) getModel();
            setModel(new DefaultTableModel(new Object[][] { { "Device", deviceNameInfo, }, { "Host", hostTableName },
                    { "Version IDL", version }, { "Server Name", serverTableName }, { "Server PID", serveurPID },
                    { "Started", exported }, { "Last started", lastExported }, { "Last stopped", lastUnexported } },
                    new String[] { ObjectUtils.EMPTY_STRING, "Device information" }));
            getColumnModel().getColumn(0).setCellRenderer(new CustomCellRenderer());
            tableModel.fireTableStructureChanged();
            setColumnWidth(0, 120);
        } else {
            setModel(new DefaultTableModel(new Object[][] {}, new String[] { "No Information" }));
            DefaultTableModel tableModel = (DefaultTableModel) getModel();
            tableModel.fireTableStructureChanged();
            setColumnWidth(0, 120);
        }
    }

    private void setColumnWidth(int col, int witdh) {
        TableColumnModel columnModel = getColumnModel();
        if (columnModel != null) {
            TableColumn column = columnModel.getColumn(col);
            column.setMaxWidth(witdh);
            column.setMinWidth(witdh);
            column.setResizable(false);
        }
    }

    public class CustomCellRenderer extends DefaultTableCellRenderer {

        private static final long serialVersionUID = 6005577074308981659L;

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                int row, int column) {

            Component rendererComp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                    column);

            return rendererComp;
        }

    }

    @Override
    public void systemControlChanged(final CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.SELECTED_DEVICE, eventType)) {
            String deviceName = String.valueOf(event.getInformation());
            DeviceModel deviceModel = controller.getDeviceModel(deviceName);
            if (deviceModel != null) {
                updateTableInformation(deviceModel);
            } else {
                updateTableInformation(null);
            }
        }
    }

}