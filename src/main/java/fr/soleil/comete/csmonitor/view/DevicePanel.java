package fr.soleil.comete.csmonitor.view;

import java.awt.CardLayout;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.soleil.comete.csmonitor.controller.CSEvent;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.controller.ICSMonitorListener;
import fr.soleil.comete.csmonitor.view.DevicesTable.TableType;
import fr.soleil.lib.project.ObjectUtils;

public class DevicePanel extends JPanel implements ICSMonitorListener {

    private static final long serialVersionUID = -4636334478346181077L;

    private static enum LAYOUT {
        EMPTY_PANEL, DEVICE_PANEL
    };

    private ICSMonitor controller = null;
    private DevicePopupMenu popupMenu = null;

    private JPanel emptyPanel = null;
    private JPanel tablePanel = null;

    private CardLayout cardLayout = null;

    private JSplitPane mainSplitPane = null;
    private JSplitPane northSplitPane = null;
    private JSplitPane southSplitPane = null;

    private DeviceInformationTable informationTable = null;
    private DevicesTable subDevicesTable = null;
    private SubdevicesTree subdevicesTreePanel = null;
    private DevicesTable relatedDeviceTable = null;
    private DeviceStatePanel deviceState = null;

    public DevicePanel(ICSMonitor controller, DevicePopupMenu popupMenu) {
        this.controller = controller;
        this.popupMenu = popupMenu;
        initComponent();
    }

    private void initComponent() {
        controller.setLoadingMessage("Create information table");
        informationTable = new DeviceInformationTable(controller);
        subDevicesTable = new DevicesTable(controller, TableType.SUBDEVICE, popupMenu);
        subdevicesTreePanel = new SubdevicesTree(controller, popupMenu);
        relatedDeviceTable = new DevicesTable(controller, TableType.RELATED, popupMenu);
        controller.setLoadingMessage("Create state panel");
        deviceState = new DeviceStatePanel(controller);

        JScrollPane informationScrollPane = new JScrollPane(informationTable);
        JScrollPane relatedDeviceScrollPane = new JScrollPane(relatedDeviceTable);

        boolean showSubdevicesPanel = PreferenceFile.getShowSubdevicesPanel();

        if (showSubdevicesPanel) {
            southSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, subDevicesTable, relatedDeviceScrollPane);
        } else {
            southSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, new JScrollPane(subdevicesTreePanel),
                    relatedDeviceScrollPane);
        }

        northSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, deviceState, informationScrollPane);
        northSplitPane.setOneTouchExpandable(true);

        southSplitPane.setOneTouchExpandable(true);

        mainSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, northSplitPane, southSplitPane);
        mainSplitPane.setOneTouchExpandable(true);

        resetSplitPaneLocation();

        tablePanel = new JPanel(new GridLayout(1, 1));
        tablePanel.add(mainSplitPane);

        emptyPanel = new JPanel();

        cardLayout = new CardLayout();
        setLayout(cardLayout);

        add(tablePanel, LAYOUT.DEVICE_PANEL.toString());
        add(emptyPanel, LAYOUT.EMPTY_PANEL.toString());

        cardLayout.show(this, LAYOUT.EMPTY_PANEL.toString());

        controller.addCSMonitorListener(this);
    }

    public void saveSplitPaneLocation() {
        PreferenceFile.setNorthDividerLocation(northSplitPane.getDividerLocation());
        PreferenceFile.setSouthDividerLocation(southSplitPane.getDividerLocation());
        PreferenceFile.setCenterDividerLocation(mainSplitPane.getDividerLocation());
    }

    public void resetSplitPaneLocation() {
        northSplitPane.setDividerLocation(PreferenceFile.getNorthDividerLocation());
        southSplitPane.setDividerLocation(PreferenceFile.getSouthDividerLocation());
        mainSplitPane.setDividerLocation(PreferenceFile.getCenterDividerLocation());
    }

    @Override
    public void systemControlChanged(CSEvent event) {
        EventType eventType = event.getEventType();
        if (ObjectUtils.sameObject(EventType.SELECTED_DEVICE, eventType)) {
            String deviceName = String.valueOf(event.getInformation());

            if (deviceName.equals("null")) {
                cardLayout.show(this, LAYOUT.EMPTY_PANEL.toString());
            } else {
                cardLayout.show(this, LAYOUT.DEVICE_PANEL.toString());
            }
        }
    }

    public void changeSubdevicesPanel(boolean isTreeActif) {
        southSplitPane.remove(0);
        if (!isTreeActif) {
            southSplitPane.add(subdevicesTreePanel, 0);

        } else {
            southSplitPane.add(subDevicesTable, 0);
        }
        southSplitPane.repaint();
    }
}
