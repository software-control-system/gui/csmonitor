package fr.soleil.comete.csmonitor.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import fr.esrf.Tango.DevState;
import fr.soleil.comete.csmonitor.controller.ICSMonitor;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.comete.csmonitor.util.TangoColorUtil;
import fr.soleil.comete.swing.util.CometeTreeCellRenderer;

public class SubdevicesTreeRenderer extends CometeTreeCellRenderer {

    private static final long serialVersionUID = 4313989374108164212L;

    private ICSMonitor controller = null;

    public SubdevicesTreeRenderer(ICSMonitor controller) {
        this.controller = controller;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        Component superComponent = super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row,
                hasFocus);

        DefaultMutableTreeNode defaultTreeNode = null;
        if (value instanceof DefaultMutableTreeNode) {
            defaultTreeNode = (DefaultMutableTreeNode) value;

            boolean root = ((DefaultMutableTreeNode) value).isRoot();
            if (root) {

            } else {
                ImageIcon icon = null;
                Color colorForState = Color.BLACK;

                DeviceModel deviceModel = controller.getDeviceModel(defaultTreeNode.toString());
                if (deviceModel != null) {
                    DevState deviceState = deviceModel.getState();
                    if (deviceState == DevState.UNKNOWN) {
                        colorForState = TangoColorUtil.getColorForState(deviceState.toString());
                    }
                    icon = ResourcesUtil.getIconForState(deviceState);
                    if (superComponent != null && superComponent instanceof JLabel) {
                        superComponent.setForeground(colorForState);
                        if (icon != null) {
                            JLabel label = (JLabel) superComponent;
                            label.setIcon(icon);
                        }
                    }
                }
            }
        }
        return superComponent;
    }
}
