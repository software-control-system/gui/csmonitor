package fr.soleil.comete.csmonitor.util;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import fr.esrf.Tango.DevState;
import fr.soleil.comete.csmonitor.controller.ICSMonitor.NodeState;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.swing.icons.DecorableIcon;

public class ResourcesUtil {

    public static final ResourceBundle MESSAGES = ResourceBundle.getBundle("fr.soleil.comete.csmonitor.messages");
    public static final ResourceBundle RESOURCES = ResourceBundle.getBundle("fr.soleil.comete.csmonitor.icons");
    public static final DateTimeFormatter DATE_FORMAT = DateUtil.getDateTimeFormatter("yyyy/MM/dd HH:mm:ss");

    public static CometeImage getImage(String key) {
        URL file = ResourcesUtil.class.getResource(RESOURCES.getString(key));
        return new CometeImage(file.toString());
    }

    public static ImageIcon getIcon(String key) {
        URL file = ResourcesUtil.class.getResource(RESOURCES.getString(key));
        ImageIcon icon = new ImageIcon(file);
        return icon;
    }

    public static DecorableIcon getDecorableIcon(String key, String decoration) {
        URL file = ResourcesUtil.class.getResource(RESOURCES.getString(key));
        URL decorationFile = ResourcesUtil.class.getResource(RESOURCES.getString(decoration));
        DecorableIcon icon = new DecorableIcon(file);
        icon.setDecoration(new ImageIcon(decorationFile));
        icon.setDecorationHorizontalAlignment(SwingConstants.RIGHT);
        icon.setDecorationVerticalAlignment(SwingConstants.BOTTOM);
        icon.setDecorated(true);
        return icon;
    }

    private static String getCompleteFileJarName(Class<?> clazz) {
        String completeJarFilename = null;
        if (clazz != null) {
            CodeSource source = clazz.getProtectionDomain().getCodeSource();
            URL location = source.getLocation();
            String path = location.getPath();

            try {
                String decodedPath = URLDecoder.decode(path, "UTF-8");
                File file = new File(decodedPath);
                String name = file.getName();

                if (name.endsWith(".jar")) {
                    completeJarFilename = name;
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return completeJarFilename;
    }

    public static String getFileJarVersion(Class<?> clazz) {
        String version = null;
        if (clazz != null) {
            String jarFileName = getCompleteFileJarName(clazz);
            if ((jarFileName != null) && !jarFileName.isEmpty()) {
                int indexEnd = jarFileName.lastIndexOf(".jar");
                int indexBegin = jarFileName.lastIndexOf("-");
                if ((indexEnd > -1) && (indexBegin > -1)) {
                    version = jarFileName.substring(indexBegin + 1, indexEnd);
                }
            }
        }
        return version;
    }

    public static ImageIcon getIconForState(final DevState state) {
        ImageIcon icon = ResourcesUtil.getIcon("DeviceTree.LED_UNKNOWN");
        switch (state.value()) {
            case DevState._ON:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_ON");
                break;
            case DevState._OFF:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_OFF");
                break;
            case DevState._OPEN:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_OPEN");
                break;
            case DevState._INSERT:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_INSERT");
                break;
            case DevState._EXTRACT:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_EXTRACT");
                break;
            case DevState._MOVING:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_MOVING");
                break;
            case DevState._STANDBY:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_STANDBY");
                break;
            case DevState._FAULT:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_FAULT");
                break;
            case DevState._INIT:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_INIT");
                break;
            case DevState._RUNNING:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_RUNNING");
                break;
            case DevState._ALARM:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_ALARM");
                break;
            case DevState._DISABLE:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_DISABLE");
                break;
            default:
                icon = ResourcesUtil.getIcon("DeviceTree.LED_UNKNOWN");
                break;
        }
        return icon;
    }

    public static ImageIcon getIconForNodeState(final NodeState state) {
        ImageIcon icon = ResourcesUtil.getIcon("IconForNodeState.CANCEL");
        switch (state) {
            case FAULT:
                icon = ResourcesUtil.getIcon("IconForNodeState.ERROR");
                break;
            case OK:
                icon = ResourcesUtil.getIcon("IconForNodeState.ACCEPT");
                break;
            default:
                icon = ResourcesUtil.getIcon("IconForNodeState.CANCEL");
                break;
        }
        return icon;
    }

    public static void runInEDT(final Runnable runnable) {
        EDTManager.INSTANCE.runInDrawingThread(runnable);
    }

    public static <E> E getFirstElement(Collection<E> list) {
        E element = null;
        if (list != null) {
            for (E e : list) {
                element = e;
                break;
            }
        }
        return element;
    }

}
