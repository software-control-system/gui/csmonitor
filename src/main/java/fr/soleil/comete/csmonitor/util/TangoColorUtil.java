package fr.soleil.comete.csmonitor.util;

import java.awt.Color;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.tango.data.adapter.StateAdapter;

public class TangoColorUtil extends StateAdapter<String> {
    private static final TangoColorUtil instance = new TangoColorUtil();

    public TangoColorUtil() {
        super(null, String.class);
        try {
            initEncoding();
        } catch (Exception e) {

        }
    }

    public static Color getColorForState(String state) {
        Color color = null;
        CometeColor cometeColor = instance.encodingMap.get(state);
        if (cometeColor != null) {
            color = ColorTool.getColor(cometeColor);
        }
        return color;
    }
}
