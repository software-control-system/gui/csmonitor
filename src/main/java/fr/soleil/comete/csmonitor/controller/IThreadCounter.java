package fr.soleil.comete.csmonitor.controller;

public interface IThreadCounter {

    public boolean isThreadFinished();
    
    public void startCount();
    
}
