package fr.soleil.comete.csmonitor.controller;

import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.csmonitor.controller.CSEvent.EventType;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.comete.csmonitor.model.ServerSource;
import fr.soleil.comete.csmonitor.util.ResourcesUtil;
import fr.soleil.comete.csmonitor.view.WaitingDialog;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.date.DateUtil;
import fr.soleil.lib.project.swing.Splash;

public abstract class AbstractCSMonitor implements ICSMonitor {

    protected static final Logger LOGGER = LoggerFactory.getLogger(ICSMonitor.class);

    private static final Splash SPLASH = new Splash(ResourcesUtil.getIcon("CSMonitor.Splash"), Color.WHITE);
    static {
        SPLASH.setTitle(ResourcesUtil.MESSAGES.getString("CSMonitor.Splash.Application.Title"));
        SPLASH.setCopyright(ResourcesUtil.MESSAGES.getString("CSMonitor.Splash.Application.Copyright"));
        SPLASH.setMessage(ResourcesUtil.MESSAGES.getString("CSMonitor.Splash.Application.Message"));
        SPLASH.progress(0);
        SPLASH.setAlwaysOnTop(true);
    }

    private int loadingProgress;
    private ServerSource serveurSource;
    private WaitingDialog waitingDialog;
    private DeviceProxy controlProxy;
    private Collection<ICSMonitorListener> listeners;
    private Map<String, Collection<String>> nodeMap;

    public AbstractCSMonitor() {
        super();
        loadingProgress = 0;
        listeners = new ArrayList<>();
        nodeMap = new HashMap<>();
        setLoadingMessage(this.getClass().getName() + " creation...");
        increaseLoadingProgress(2);
    }

    @Override
    public void showWaitingDialog(final Component parent, final String message) {
        if (waitingDialog == null) {
            waitingDialog = new WaitingDialog(parent);
        }

        ResourcesUtil.runInEDT(new Runnable() {
            @Override
            public void run() {
                if (waitingDialog != null) {
                    if (message != null) {
                        waitingDialog.setMainMessage(message);
                    }
                    if (parent == null) {
                        waitingDialog.setVisible(true);
                    }
                }
            }
        });
    }

    @Override
    public void hideWaitingDialog() {
        if (waitingDialog != null) {
            ResourcesUtil.runInEDT(new Runnable() {
                @Override
                public void run() {
                    waitingDialog.setVisible(false);
                }
            });
        }
    }

    @Override
    public void addCSMonitorListener(ICSMonitorListener listener) {
        if (listener != null && !listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeCSMonitorListener(ICSMonitorListener listener) {
        if (listener != null && listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    @Override
    public void fireSystemControlChanged(final CSEvent event) {
        for (ICSMonitorListener listener : listeners) {
            listener.systemControlChanged(event);
        }
    }

    @Override
    public ISourceDevice getServerSource() {
        if (serveurSource == null) {
            serveurSource = new ServerSource(this);
        }
        return serveurSource;
    }

    @Override
    public void increaseLoadingProgress(int percent) {
        if (loadingProgress < 100) {
            loadingProgress = loadingProgress + percent;
            // System.out.println("loadingProgress" + loadingProgress);
            SPLASH.progress(loadingProgress);
        }

    }

    @Override
    public void setLoadingMessage(String message) {
        SPLASH.setMessage(message);
    }

    @Override
    public void setLoadingMessageVisible(boolean visible) {
        increaseLoadingProgress(100 - loadingProgress);
        SPLASH.setVisible(visible);
    }

    @Override
    public String getDeviceName(final String nodeName) {
        String deviceName = null;

        ISourceDevice serverSource = getServerSource();
        ISourceDevice deviceSource = getDeviceSource();

        if (nodeName != null && deviceSource != null && serverSource != null) {
            String deviceSeparator = deviceSource.getPathSeparator();
            String serverSeparator = serverSource.getPathSeparator();
            String[] split = nodeName.split(deviceSeparator);
            if (split != null) {
                if (split.length == 3) {
                    // It is the server source
                    if (nodeName.contains(serverSeparator)) {
                        int lastIndexOf = nodeName.lastIndexOf(serverSeparator);
                        deviceName = nodeName.substring(lastIndexOf + 1, nodeName.length());
                    } else {
                        deviceName = nodeName.toLowerCase();
                    }
                }
            }
        }
        return deviceName;
    }

    @Override
    public DeviceModel getDeviceModel(final String deviceName) {
        return DeviceModel.getDeviceModel(deviceName, this);
    }

    @Override
    public void repairDevice(final Collection<String> deviceList, final IThreadCounter counter) {
        if (deviceList != null) {
            this.showWaitingDialog(null, "repairDevice " + deviceList);
            Thread thread = new Thread("initDevice") {
                @Override
                public void run() {
                    IThreadCounter acounter = counter;
                    if (counter == null) {
                        acounter = new ThreadCounter(3000);
                    }
                    for (String device : deviceList) {
                        repairDevice(device, acounter);
                    }
                    hideWaitingDialog();
                    refreshDevice(deviceList);
                }
            };
            thread.start();
        }
    }

    private void repairDevice(final String deviceName, final IThreadCounter counter) {
        if (deviceName != null && !deviceName.isEmpty()) {
            logMessage(this.getClass().getSimpleName(), "repairDevice " + deviceName);

            // Step 1 - Check device state before
            DeviceModel deviceModel = getDeviceModel(deviceName);
            if (deviceModel != null) {
                Collection<String> devices = new ArrayList<>();
                devices.add(deviceName);
                NodeState nodeState = getNodeState(devices);
                switch (nodeState) {
                    case FAULT:
                    case UNKNOWN:
                        // Step 2 - If FAULT Check Sub Device State and repair them one by one
                        Collection<String> subDevice = deviceModel.getSubDevice();
                        if (subDevice != null && !subDevice.isEmpty()) {
                            for (String subDev : subDevice) {
                                repairOneDevice(subDev, counter);
                            }
                        }
                        repairOneDevice(deviceName, counter);
                        break;
                    case OK:
                        logMessage(this.getClass().getSimpleName(), deviceName + "no need to be repair");
                        break;
                }
            }
        }
    }

    private void repairOneDevice(final String deviceName, final IThreadCounter counter) {
        if (deviceName != null && !deviceName.isEmpty()) {
            DeviceModel deviceModel = getDeviceModel(deviceName);
            Collection<String> devices = new ArrayList<>();
            devices.add(deviceName);
            NodeState nodeState = getNodeState(devices);
            switch (nodeState) {
                case FAULT:
                    // If Fault init device
                    initDeviceSynchrone(devices);
                    while (!nodeState.equals(NodeState.OK) || !counter.isThreadFinished()) {
                        deviceModel.refreshState();
                        nodeState = getNodeState(devices);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }
                    if (!nodeState.equals(NodeState.OK)) {
                        // Stop device start device
                        stopDevice(devices);
                        counter.startCount();
                        while (!nodeState.equals(NodeState.UNKNOWN) || !counter.isThreadFinished()) {
                            deviceModel.refreshState();
                            nodeState = getNodeState(devices);
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                        }
                        if (!nodeState.equals(NodeState.UNKNOWN)) {
                            logError(this.getClass().getSimpleName(),
                                    new Exception("Cannot Stop Server of " + deviceName));
                            break;
                        }
                    } else {
                        logMessage(this.getClass().getSimpleName(), deviceName + "successfully repair");
                        break;
                    }
                case UNKNOWN:
                    // If Unknown start device
                    startDevice(devices);
                    counter.startCount();
                    while (!nodeState.equals(NodeState.OK) || !counter.isThreadFinished()) {
                        deviceModel.refreshState();
                        nodeState = getNodeState(devices);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                        }
                    }

                    if (!nodeState.equals(NodeState.OK)) {
                        logError(this.getClass().getSimpleName(), new Exception("Cannot repair " + deviceName));
                    } else {
                        logMessage(this.getClass().getSimpleName(), deviceName + "successfully repair");
                    }

                    break;
                case OK:
                    logMessage(this.getClass().getSimpleName(), deviceName + " no need to be repaired");
                    break;
            }

        } else {
            String errorMessage = "Device Name is not defined";
            logError(this.getClass().getSimpleName(), new Exception(errorMessage));
        }

    }

    @Override
    public NodeState getNodeState(final String aNodeName, boolean deviceTree) {
        String nodeName = aNodeName;
        NodeState nodeState = NodeState.UNKNOWN;
        if (nodeName != null) {
            nodeName = nodeName.toLowerCase();
            Collection<String> deviceForNode = nodeMap.get(nodeName);
            ISourceDevice deviceSourceList = this.getDeviceSource();
            if (deviceForNode == null) {
                if (!deviceTree) {
                    deviceSourceList = this.getServerSource();
                }
                try {
                    Collection<String> sourceList = deviceSourceList.getSourceList();
                    if (sourceList != null && !sourceList.isEmpty()) {
                        deviceForNode = new ArrayList<>();
                        String serverNode = null;
                        for (String device : sourceList) {
                            device = device.toLowerCase();
                            if (!deviceTree) {
                                serverNode = nodeName.replaceFirst(ServerSource.SEPARATOR,
                                        ISourceDevice.DEFAULT_SEPARATOR);
                                deviceForNode = ((ServerSource) deviceSourceList).getDeviceListForServer(serverNode);
                            } else {
                                if (isRootNode(device, aNodeName)) {
                                    if (!deviceForNode.contains(device)) {
                                        deviceForNode.add(device);
                                    }
                                }
                            }

                            if (!deviceForNode.isEmpty()) {
                                break;
                            }
                        }
                        nodeMap.put(nodeName.toLowerCase(), deviceForNode);
                    }
                } catch (SourceDeviceException e) {
                    logError(this.getClass().getSimpleName(), e);
                }

            }

            nodeState = getNodeState(deviceForNode);
        }

        return nodeState;
    }

    @Override
    public NodeState getNodeState(final Collection<String> deviceList) {
        NodeState nodeState = NodeState.UNKNOWN;

        if (deviceList != null) {
            boolean isOneUnknown = false;
            boolean isOneInFault = false;
            for (String devName : deviceList) {
                DeviceModel deviceModel = this.getDeviceModel(devName);
                if (deviceModel != null) {
                    DevState state = deviceModel.getState();
                    switch (state.value()) {
                        case DevState._UNKNOWN:
                            isOneUnknown = true;
                            break;
                        case DevState._FAULT:
                        case DevState._ALARM:
                        case DevState._DISABLE:
                        case DevState._INIT:
                            isOneInFault = true;
                            break;
                        default:
                            break;
                    }
                }
                // Do not continue the process
                if (isOneUnknown) {
                    break;
                }
            }

            if (isOneUnknown) {
                nodeState = NodeState.UNKNOWN;
            } else if (isOneInFault) {
                nodeState = NodeState.FAULT;
            } else {
                nodeState = NodeState.OK;
            }
        }

        return nodeState;
    }

    @Override
    public boolean isOneDeviceRunning(final Collection<String> deviceList) {
        boolean running = false;
        if (deviceList != null && !deviceList.isEmpty()) {
            for (String devName : deviceList) {
                DeviceModel deviceModel = this.getDeviceModel(devName);
                if (deviceModel != null) {
                    DevState state = deviceModel.getState();
                    if (!DevState.UNKNOWN.equals(state)) {
                        running = true;
                        break;
                    }
                }
            }
        }
        return running;
    }

    @Override
    public boolean isOneDeviceStartable(final Collection<String> deviceList) {
        boolean startable = false;
        if (isOneDeviceUnknown(deviceList)) {
            if (deviceList != null && !deviceList.isEmpty()) {
                for (String devName : deviceList) {
                    DeviceModel deviceModel = this.getDeviceModel(devName);
                    if (deviceModel != null) {
                        if (deviceModel.isStartable()) {
                            startable = true;
                            break;
                        }
                    }
                }
            }
        }

        return startable;
    }

    @Override
    public boolean isOneDeviceFault(final Collection<String> deviceList) {
        boolean fault = false;
        if (deviceList != null && !deviceList.isEmpty()) {
            for (String devName : deviceList) {
                DeviceModel deviceModel = this.getDeviceModel(devName);
                if (deviceModel != null) {
                    DevState state = deviceModel.getState();
                    if (DevState.FAULT.equals(state) || DevState.ALARM.equals(state) || DevState.DISABLE.equals(state)
                            || DevState.INIT.equals(state)) {
                        fault = true;
                        break;
                    }
                }
            }
        }
        return fault;

    }

    @Override
    public boolean isOneDeviceUnknown(final Collection<String> deviceList) {
        boolean unknown = false;
        if (deviceList != null && !deviceList.isEmpty()) {
            for (String devName : deviceList) {
                DeviceModel deviceModel = this.getDeviceModel(devName);
                if (deviceModel != null) {
                    DevState state = deviceModel.getState();
                    if (DevState.UNKNOWN.equals(state)) {
                        unknown = true;
                        break;
                    }
                }
            }
        }
        return unknown;
    }

    private DeviceProxy getControlProxy() {
        if (controlProxy == null) {
            Database database = TangoDeviceHelper.getDatabase();
            if (database != null) {
                try {
                    String[] get_device_exported_for_class = database
                            .get_device_exported_for_class("ControlSystemMonitor");
                    if (get_device_exported_for_class != null && get_device_exported_for_class.length > 0) {
                        controlProxy = TangoDeviceHelper.getDeviceProxy(get_device_exported_for_class[0]);
                    }
                } catch (DevFailed e) {
                    this.logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(e)));
                }

                if (controlProxy == null) {
                    this.logError(this.getClass().getSimpleName(),
                            new Exception("No ControlSystemMonitor device found"));
                }
            }
        }
        return controlProxy;
    }

    @Override
    public void startDevice(final Collection<String> deviceList) {
        if (deviceList != null && !deviceList.isEmpty()) {
            final DeviceProxy controlProxy2 = getControlProxy();
            if (controlProxy2 != null) {
                this.showWaitingDialog(null, "startDevice...");
                this.logMessage(this.getClass().getSimpleName(), "startDevice " + deviceList);
                Thread thread = new Thread("startDevice") {
                    @Override
                    public void run() {

                        Collection<String> serverListForDevice = getServerListForDevice(deviceList, true);
                        for (String server : serverListForDevice) {
                            try {
                                showWaitingDialog(null, "start server " + server);
                                DeviceData deviceData = new DeviceData();
                                deviceData.insert(server);
                                controlProxy2.command_inout("StartServer", deviceData);
                            } catch (DevFailed devFailed) {
                                logError(this.getClass().getSimpleName(),
                                        new Exception(DevFailedUtils.toString(devFailed)));
                            }

                        }
                        refreshDevice(deviceList);
                        hideWaitingDialog();
                    }
                };
                thread.start();
            }
        }
    }

    private Collection<String> getServerListForDevice(final Collection<String> deviceList, boolean start) {
        Collection<String> serverList = new ArrayList<>();
        DeviceModel deviceModel = null;
        DeviceInfo deviceInfo = null;
        String server = null;
        for (String device : deviceList) {
            deviceModel = getDeviceModel(device);
            if (deviceModel != null) {
                DevState state = deviceModel.getState();
                if (!(DevState.UNKNOWN.equals(state) ^ start)) {
                    deviceInfo = deviceModel.getDeviceInfo();
                    if (deviceInfo != null) {
                        server = deviceInfo.server;
                        if (server != null && !server.isEmpty() && !serverList.contains(server)) {
                            serverList.add(server);
                        }
                    }
                }
            }
        }
        return serverList;
    }

    @Override
    public void stopDevice(final Collection<String> deviceList) {
        if (deviceList != null && !deviceList.isEmpty()) {
            final DeviceProxy controlProxy2 = getControlProxy();
            if (controlProxy2 != null) {
                this.showWaitingDialog(null, "stopDevice...");
                this.logMessage(this.getClass().getSimpleName(), "stopDevice " + deviceList);
                Thread thread = new Thread("stopDevice") {
                    @Override
                    public void run() {

                        Collection<String> serverListForDevice = getServerListForDevice(deviceList, false);
                        for (String server : serverListForDevice) {
                            try {
                                showWaitingDialog(null, "stop server " + server);
                                DeviceData deviceData = new DeviceData();
                                deviceData.insert(server);
                                controlProxy2.command_inout("StopServer", deviceData);
                            } catch (DevFailed devFailed) {
                                logError(this.getClass().getSimpleName(),
                                        new Exception(DevFailedUtils.toString(devFailed)));
                            }

                        }
                        refreshDevice(deviceList);
                        hideWaitingDialog();
                    }
                };
                thread.start();
            }
        }

    }

    @Override
    public void refreshDevice(final Collection<String> deviceList) {
        if (deviceList != null) {
            for (String device : deviceList) {
                DeviceModel deviceModel = this.getDeviceModel(device);
                if (deviceModel != null) {
                    deviceModel.refreshInfo();
                }
            }
        }
        refreshState();
    }

    @Override
    public void refreshState() {
        DeviceModel.refreshAllInfo();
    }

    @Override
    public String getControlMonitorDevice() {
        String deviceName = null;
        final DeviceProxy controlProxy2 = getControlProxy();
        if (controlProxy2 != null) {
            deviceName = controlProxy2.get_name();
        }
        return deviceName;
    }

    @Override
    public void initDevice(final Collection<String> deviceList) {
        if (deviceList != null && !deviceList.isEmpty()) {
            this.showWaitingDialog(null, "init Device " + deviceList);
            Thread thread = new Thread("initDevice") {
                @Override
                public void run() {
                    initDeviceSynchrone(deviceList);
                    refreshDevice(deviceList);
                    hideWaitingDialog();
                }
            };
            thread.start();
        }

    }

    private void initDeviceSynchrone(final Collection<String> deviceList) {
        for (String device : deviceList) {
            try {
                DeviceModel deviceModel = getDeviceModel(device);
                if (deviceModel != null && !DevState.UNKNOWN.equals(deviceModel.getState())) {
                    showWaitingDialog(null, "init device " + device);
                    DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(device);
                    if (deviceProxy != null) {
                        deviceProxy.command_inout("Init");
                    }
                }
            } catch (DevFailed devFailed) {
                logError(this.getClass().getSimpleName(), new Exception(DevFailedUtils.toString(devFailed)));
            }
        }
    }

    @Override
    public boolean isRootNode(final String aRootName, final String aNodeName) {
        boolean rootNode = false;

        if (aNodeName != null && aRootName != null) {
            String rootName = aNodeName.toLowerCase();
            String nodeName = aRootName.toLowerCase();

            String[] splitRootName = rootName.split(ISourceDevice.DEFAULT_SEPARATOR);
            String[] splitNodeName = nodeName.split(ISourceDevice.DEFAULT_SEPARATOR);

            if (splitRootName != null && splitNodeName != null && splitNodeName.length >= splitRootName.length) {
                boolean same = true;
                for (int i = 0; i < splitRootName.length; i++) {
                    if (!splitRootName[i].equals(splitNodeName[i])) {
                        same = false;
                        break;
                    }
                }
                rootNode = same;
            }
        }
        return rootNode;
    }

    @Override
    public void setDeviceSelected(String deviceName, boolean deviceTree) {
        fireSystemControlChanged(new CSEvent(deviceTree, deviceName, EventType.SET_DEVICE_SELECTED));
    }

    @Override
    public void logMessage(final Object source, final String message) {
        String date = getDate();
        fireSystemControlChanged(new CSEvent(source, message, EventType.LOG_CHANGED));
        LOGGER.error("INFO from {} at {} : {}", String.valueOf(source), date, message);
    }

    @Override
    public void logError(final Object source, final Exception error) {
        String date = getDate();
        fireSystemControlChanged(new CSEvent(source, date + "ERROR " + error.getMessage(), EventType.LOG_CHANGED));
        LOGGER.error(String.format("ERROR from %s at %s : %s", String.valueOf(source), date, error.getMessage()),
                error);
    }

    private String getDate() {
        return DateUtil.milliToString(System.currentTimeMillis(), DATE_FORMAT);
    }

}
