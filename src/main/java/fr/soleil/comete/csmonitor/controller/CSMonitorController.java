package fr.soleil.comete.csmonitor.controller;

import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.csmonitor.model.DeviceSource;

public class CSMonitorController extends AbstractCSMonitor {

    private DeviceSource deviceSource = null;

    public CSMonitorController() {
        super();
        deviceSource = new DeviceSource(this);
    }

    public CSMonitorController(String argDeviceName) {
        super();
        deviceSource = new DeviceSource(this, argDeviceName);
    }

    @Override
    public ISourceDevice getDeviceSource() {
        return deviceSource;
    }

}
