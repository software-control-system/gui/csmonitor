package fr.soleil.comete.csmonitor.controller;

public interface ICSMonitorListener {
    
    public void systemControlChanged(final CSEvent event);
    
}
