package fr.soleil.comete.csmonitor.controller;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.bean.tangotree.SourceDeviceException;
import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;

public class SimulatedControlMonitor extends AbstractCSMonitor implements ISourceDevice {

    private static final String NAME = SimulatedControlMonitor.class.getSimpleName();
    private static final String INFO = "Simulated tango Data base";
    private final List<String> deviceList;

    public SimulatedControlMonitor() {
        super();
        deviceList = new ArrayList<>();
        deviceList.add("tango/tangotest/1");
        deviceList.add("tango/tangotest/2");
        deviceList.add("tango/tangotest/3");
        deviceList.add("tango/tangotest/4");
        deviceList.add("ans/ca/machinestatus");
        deviceList.add("ans-c01/ca/pm_sampling_clock");
        deviceList.add("storage/recorder/datarecorder.1");
        deviceList.add("storage/test/authserver.1");
        deviceList.add("storage/test/experimentalframe.1");
        deviceList.add("ica/salsa/scan.1");
        deviceList.add("ica/salsa/scan.2");
        deviceList.add("tests/labo/v260-A");
        deviceList.add("test/enumerated/enumatt_spjz1");
        // deviceList.add("test/anthony/vanne.1");
        deviceList.add("det/mos/cb");
        deviceList.add("det/mos/axis-a");
        deviceList.add("tmp/test/delta");
        deviceList.add("tmp/test/gamma");
        deviceList.add("tmp/test/kappa");

    }

    @Override
    public ISourceDevice getDeviceSource() {
        return this;
    }

    @Override
    public List<String> getSourceList() throws SourceDeviceException {
        return deviceList;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getInformation() {
        return INFO;
    }

    @Override
    public String getPathSeparator() {
        return ISourceDevice.DEFAULT_SEPARATOR;
    }

}
