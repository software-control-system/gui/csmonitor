package fr.soleil.comete.csmonitor.controller;

import java.util.EventObject;

public class CSEvent extends EventObject {

    private static final long serialVersionUID = -8019940341267855998L;

    public static enum EventType {
        SELECTED_DEVICE, LOG_CHANGED, STATE_REFRESHED, SET_DEVICE_SELECTED
    };

    private EventType eventType = EventType.SELECTED_DEVICE;
    private Object information = null;

    public CSEvent(Object source, Object information, EventType eventType) {
        super(source);
        this.information = information;
        this.eventType = eventType;
    }

    public Object getInformation() {
        return information;
    }

    public EventType getEventType() {
        return eventType;
    }

}
