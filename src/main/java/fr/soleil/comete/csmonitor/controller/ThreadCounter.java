package fr.soleil.comete.csmonitor.controller;

public class ThreadCounter implements IThreadCounter {

    private long startTime;
    private long waitingTime;

    public ThreadCounter(long waitingTime) {
        this.startTime = 0;
        this.waitingTime = waitingTime;
    }

    public void setWaitingTime(long waitingTime) {
        this.waitingTime = waitingTime;
    }

    @Override
    public void startCount() {
        startTime = System.currentTimeMillis();
    }

    @Override
    public boolean isThreadFinished() {
        return System.currentTimeMillis() - startTime > waitingTime;
    }
}
