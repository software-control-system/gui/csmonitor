package fr.soleil.comete.csmonitor.controller;

import java.awt.Component;
import java.time.format.DateTimeFormatter;
import java.util.Collection;

import fr.soleil.comete.bean.tangotree.datasource.ISourceDevice;
import fr.soleil.comete.csmonitor.model.DeviceModel;
import fr.soleil.lib.project.date.DateUtil;

public interface ICSMonitor {

    public static enum NodeState {
        FAULT, OK, UNKNOWN
    };

    public static final DateTimeFormatter DATE_FORMAT = DateUtil.getDateTimeFormatter("dd-MM-yyyy HH:mm:ss");

    public void addCSMonitorListener(ICSMonitorListener listener);

    public void removeCSMonitorListener(ICSMonitorListener listener);

    public String getControlMonitorDevice();

    public ISourceDevice getDeviceSource();

    public ISourceDevice getServerSource();

    public DeviceModel getDeviceModel(final String deviceName);

    public void fireSystemControlChanged(final CSEvent event);

    public void increaseLoadingProgress(int percent);

    public void setLoadingMessage(final String message);

    public void setLoadingMessageVisible(boolean visible);

    public String getDeviceName(final String nodeName);

    public NodeState getNodeState(final String nodeName, boolean deviceTree);

    public NodeState getNodeState(final Collection<String> deviceList);

    public boolean isOneDeviceRunning(final Collection<String> deviceList);

    public boolean isOneDeviceUnknown(final Collection<String> deviceList);

    public boolean isOneDeviceFault(final Collection<String> deviceList);

    public boolean isOneDeviceStartable(final Collection<String> deviceList);

    public void startDevice(final Collection<String> deviceList);

    public void stopDevice(final Collection<String> deviceList);

    public void refreshDevice(final Collection<String> deviceList);

    public void repairDevice(final Collection<String> deviceList, IThreadCounter counter);

    public void refreshState();

    public void initDevice(final Collection<String> deviceList);

    public void logMessage(final Object source, final String message);

    public void logError(final Object source, final Exception error);

    public void showWaitingDialog(final Component parent, String message);

    public void hideWaitingDialog();

    public boolean isRootNode(final String arootName, final String anodeName);

    public void setDeviceSelected(String deviceName, boolean deviceTree);

}
